// Define study
const study = lab.util.fromObject({
  "title": "root",
  "type": "lab.flow.Sequence",
  "parameters": {},
  "plugins": [
    {
      "type": "lab.plugins.Metadata",
      "path": undefined
    },
    {
      "type": "lab.plugins.PostMessage",
      "path": undefined
    }
  ],
  "metadata": {
    "title": "Facebook Post",
    "description": "",
    "repository": "",
    "contributors": ""
  },
  "messageHandlers": {},
  "files": {},
  "responses": {},
  "content": [
    {
      "type": "lab.flow.Loop",
      "templateParameters": [
        {
          "Name": "Cancers",
          "Title": "Climate Change Will Give Rise to More Cancers",
          "Subtitle": "New Study Focuses on Global Impact for Major Cancers and Steps Needed to Lessen Risks",
          "Caption": "Climate change will bring an acute toll worldwide, with rising temperatures, wildfires and poor air quality, accompanied by higher rates of cancer, especially lung, skin and gastrointestinal cancers, according to a new report.",
          "Date": "05 November 2020",
          "Facebook": "Medical Xpress - Medical and Health News",
          "ArticleURL": "https:\u002F\u002Fmedicalxpress.com\u002Fnews\u002F2020-11-climate-cancers.html",
          "WebsiteURL": "medicalxpress.com",
          "FacebookURL": "medicalxpress",
          "FacebookType": "Medical & health",
          "WikipediaURL": "Phys.org",
          "WikipediaCaption": "Phys.org is a UK-based science, research and technology news aggregator offering briefs from press releases and news agencies. It also summarizes journal reports and produces its own science journalism.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "medical-xpress.png",
          "Image": "https:\u002F\u002Fscx2.b-cdn.net\u002Fgfx\u002Fnews\u002Fhires\u002F2020\u002F2-cancer.jpg"
        },
        {
          "Name": "CivilizationB",
          "Title": "New Report Warns “High Likelihood Of Human Civilization Coming To An End” Within 30 Years",
          "Subtitle": "A new report warns of an apocalyptic scenario that takes mankind “beyond the threshold of human survivability” across much of our planet by 2050.",
          "Caption": "Their analysis concludes that there is a “high likelihood of human civilization coming to an end” and the time frame for this climate crisis on Earth is only 30 years into the future.",
          "Date": "05 June 2019",
          "Facebook": "ScienceVibe",
          "ArticleURL": "https:\u002F\u002Fsciencevibe.com\u002F2019\u002F06\u002F05\u002Fnew-report-warns-high-likelihood-of-human-civilization-coming-to-an-end-within-30-years\u002F",
          "WebsiteURL": "sciencevibe.com",
          "FacebookURL": "sciencevibe",
          "FacebookType": "Science website",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 5 years ago",
          "Logo": "sciencevibe.jpg",
          "Image": "https:\u002F\u002Favatars.mds.yandex.net\u002Fget-zen_doc\u002F230574\u002Fpub_5ae77fc5a815f19a06c01edd_5ae780ca1aa80cd30aa7525f\u002Fscale_1200"
        },
        {
          "Name": "Miscarriage",
          "Title": "Number of women to lose their unborn child after having the Covid Vaccine increases by 366% in just six weeks",
          "Subtitle": "Losing a new born is a heart breaking endeavour, as is the pain of losing an unborn child.",
          "Caption": "According to the seventh report on adverse reactions to the Covid vaccines in the UK, up to the 7th of March 2021 a total of 23 women had suffered a miscarriage as a result of having the Pfizer\u002FBioNTech vaccine.",
          "Date": "27 March 2021",
          "Facebook": "Australian National Review",
          "ArticleURL": "https:\u002F\u002Faustraliannationalreview.com\u002Fhealth\u002Fnumber-of-women-to-lose-their-unborn-child-after-having-the-covid-vaccine-increases-by-366-in-just-six-weeks\u002F",
          "WebsiteURL": "australiannationalreview.com",
          "FacebookURL": "AustralianNationalReview",
          "FacebookType": "Interest",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 5 years ago",
          "Logo": "ANR.png",
          "Image": "https:\u002F\u002Faustraliannationalreview.com\u002Fwp-content\u002Fuploads\u002F2021\u002F03\u002FNumber-of-women-to-lose-their-unborn-child.jpg"
        },
        {
          "Name": "MaleC",
          "Title": "Men with Covid-19 3 times more likely to need intensive care: study",
          "Subtitle": "Men infected with Covid-19 are three times more likely to require intensive care than women and are at significantly higher risk of dying from the virus, scientists said Wednesday.",
          "Caption": "Men are almost three times more likely than women to be hospitalised in an intensive care unit and are 39 percent more likely to die from the virus, the study said.",
          "Date": "09 December 2020",
          "Facebook": "FRANCE 24 English",
          "ArticleURL": "https:\u002F\u002Fwww.france24.com\u002Fen\u002Flive-news\u002F20201209-men-with-covid-19-3-times-more-likely-to-need-intensive-care-study",
          "WebsiteURL": "france24.com",
          "FacebookURL": "FRANCE24.English",
          "FacebookType": "News and media website",
          "WikipediaURL": "France_24",
          "WikipediaCaption": "France 24 (France vingt-quatre in French and Arabic channels, France veinticuatro in Spanish channel) is a French state-owned international news television network based in Paris. Its channels broadcast in French, English, Arabic, and Spanish and are aimed at the overseas market.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "france-24.png",
          "Image": "https:\u002F\u002Fstatic.france24.com\u002Fmeta_og_twcards\u002FF24_TW.png"
        },
        {
          "Name": "Acidic",
          "Title": "Signs Your Body Is Too Acidic & Here’s What To Do To Correct It",
          "Subtitle": "Many people are already consuming enough acid forming foods, such as dairy, grains, meats, and sugar.",
          "Caption": "Acidic bodies are unhealthy bodies. When the body is overly acidic, it creates an unwanted environment in which illness, bacteria, and yeast thrive.",
          "Date": "10 September 2015",
          "Facebook": "Collective Evolution",
          "ArticleURL": "https:\u002F\u002Fwww.collective-evolution.com\u002F2015\u002F09\u002F10\u002Fsigns-your-body-is-too-acidic-how-to-correct-it\u002F",
          "WebsiteURL": "collective-evolution.com",
          "FacebookURL": "CollectiveEvolutionPage",
          "FacebookType": "Local business",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "collective-evolution.png",
          "Image": "https:\u002F\u002Fcdn2.collective-evolution.com\u002Fassets\u002Fuploads\u002F2015\u002F09\u002FScreen-Shot-2015-09-09-at-5.08.19-PM.png"
        },
        {
          "Name": "NaturalB",
          "Title": "Many plants have been “naturally GMO’d” by bacteria",
          "Subtitle": "Dozens of plants, including bananas, peanuts, hops, cranberries, and tea were found to contain the Agrobacterium microbe — the exact bacterium that scientists use to create GM crops.",
          "Caption": "Genetic modification is a process that sometimes happens naturally at the hands of bacteria, a new study concludes.",
          "Date": "21 April 2021",
          "Facebook": "ZME Science",
          "ArticleURL": "https:\u002F\u002Fwww.zmescience.com\u002Fscience\u002Fmany-plants-have-been-naturally-gmod-by-bacteria\u002F",
          "WebsiteURL": "zmescience.com",
          "FacebookURL": "zmescience",
          "FacebookType": "Science website",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "zmescience.png",
          "Image": "https:\u002F\u002Fcdn.zmescience.com\u002Fwp-content\u002Fuploads\u002F2021\u002F04\u002Fphoto-1481349518771-20055b2a7b24-1024x633.jpg"
        },
        {
          "Name": "Antibodies",
          "Title": "Why antibodies may not be the key to beating coronavirus",
          "Subtitle": "Worries over waning antibodies may be overblown, as growing evidence shows a role for T cells in the coronavirus immune response.",
          "Caption": "The body should produce both protective antibodies, which keep the virus from invading, and killer T cells, which tell virus-infected human cells to destroy themselves to keep the virus from spreading. Normally, these immune responses appear in tandem. But in a subset of those who tested positive for COVID-19, researchers found T cells but no antibodies.",
          "Date": "07 August 2020",
          "Facebook": "National Geographic",
          "ArticleURL": "https:\u002F\u002Fwww.nationalgeographic.com\u002Fscience\u002F2020\u002F08\u002Fantibodies-not-only-key-to-beating-coronavirus-cvd\u002F",
          "WebsiteURL": "nationalgeographic.com",
          "FacebookURL": "natgeo",
          "FacebookType": "Media\u002Fnews company",
          "WikipediaURL": "National_Geographic",
          "WikipediaCaption": "National Geographic is the long-lived official monthly magazine of the National Geographic Society. It is one of the most widely read magazines of all time.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "national-geographic.png",
          "Image": "https:\u002F\u002Fwww.nationalgeographic.com\u002Fcontent\u002Fdam\u002Fscience\u002F2020\u002F08\u002F07\u002Ftcell\u002Fhuman-t-cell-nih.ngsversion.1596797927303.adapt.1900.1.jpg"
        },
        {
          "Name": "Male",
          "Title": "Men with COVID-19 nearly THREE TIMES more likely to be admitted to ICUs and at higher risk of death",
          "Subtitle": "Male coronavirus patients are at greater risk of being admitted to intensive care units and dying compared to female patients, a new study suggests.",
          "Caption": "Men were 2.84 times more likely to be admitted to ICUs and 1.39 times more likely to die from COVID-19",
          "Date": "10 December 2020",
          "Facebook": "Daily Mail",
          "ArticleURL": "https:\u002F\u002Fwww.dailymail.co.uk\u002Fhealth\u002Farticle-9035089\u002FMen-COVID-19-nearly-THREE-TIMES-likely-admitted-ICUs-higher-risk-death.html",
          "WebsiteURL": "dailymail.co.uk",
          "FacebookURL": "DailyMail",
          "FacebookType": "Media\u002Fnews company",
          "WikipediaURL": "Daily_Mail",
          "WikipediaCaption": "The Daily Mail is a British daily middle-market newspaper published in London in a tabloid format. Founded in 1896, it is the United Kingdom's highest-circulated daily newspaper. Its sister paper The Mail on Sunday was launched in 1982, while Scottish and Irish editions of the daily paper were launched in 1947 and 2006 respectively. Content from the paper appears on the MailOnline website, although the website is managed separately and has its own editor.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "daily-mail.png",
          "Image": "https:\u002F\u002Fi.dailymail.co.uk\u002F1s\u002F2020\u002F12\u002F09\u002F15\u002F36640964-9035089-image-a-7_1607527830350.jpg"
        },
        {
          "Name": "Sterilization",
          "Title": "Head of Pfizer Research: Covid Vaccine is Female Sterilization",
          "Subtitle": "On December 1, 2020, the ex-Pfizer head of respiratory research Dr. Michael Yeadon and the lung specialist and former head of the public health department Dr. Wolfgang Wodarg filed an application with the EMA, the European Medicine Agency responsible for EU-wide drug approval, for the immediate suspension of all SARS CoV 2 vaccine studies, in particular the BioNtech\u002FPfizer study on BNT162b (EudraCT number 2020-002641-42).",
          "Caption": "The vaccine contains a  spike protein (see image) called  syncytin-1, vital for the formation of human placenta in women. If the vaccine works so that we form an immune response AGAINST the spike protein, we are also training the female body to attack syncytin-1, which could lead to infertility in women of an unspecified duration.",
          "Date": "03 December 2020",
          "Facebook": "Australian National Review",
          "ArticleURL": "https:\u002F\u002Faustraliannationalreview.com\u002Fhealth\u002Fhead-of-pfizer-research-covid-vaccine-is-female-sterilization\u002F",
          "WebsiteURL": "australiannationalreview.com",
          "FacebookURL": "AustralianNationalReview",
          "FacebookType": "Interest",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 5 years ago",
          "Logo": "ANR.png",
          "Image": "https:\u002F\u002Faustraliannationalreview.com\u002Fwp-content\u002Fuploads\u002F2020\u002F12\u002F5_Head-of-Pfizer-Research.jpg"
        },
        {
          "Name": "Pause",
          "Title": "Global warming ‘pause’ was a myth all along, says new study",
          "Subtitle": "The idea that global warming has “stopped” is a contrarian talking point that dates back to at least 2006.",
          "Caption": "Is there, or has there recently been, a pause or hiatus in warming? We answer with a clear and unambiguous “no”.",
          "Date": "24 November 2015",
          "Facebook": "The Conversation",
          "ArticleURL": "https:\u002F\u002Ftheconversation.com\u002Fglobal-warming-pause-was-a-myth-all-along-says-new-study-51208",
          "WebsiteURL": "theconversation.com",
          "FacebookURL": "ConversationEDU",
          "FacebookType": "Non-profit organisation",
          "WikipediaURL": "The_Conversation_%28website%29",
          "WikipediaCaption": "The Conversation is a network of not-for-profit media outlets that publish news stories on the Internet that are written by academics and researchers, under a Creative Commons — Attribution\u002FNo derivatives license.",
          "Policies": "Editorial team, Ownership and funding, Corrections, Ethics and Fact-checking",
          "Registered": "More than 10 years ago",
          "Logo": "the-conversation.jpg",
          "Image": "https:\u002F\u002Fimages.theconversation.com\u002Ffiles\u002F103011\u002Foriginal\u002Fimage-20151124-18246-1qfx4sv.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=800&fit=clip"
        },
        {
          "Name": "2000",
          "Title": "Global warming is happening at a speed and scale 'unprecedented' in the last 2,000 years, experts say",
          "Subtitle": "Global warming is happening at a speed and scale that is 'unprecedented' in the last 2,000 years, scientists say.",
          "Caption": "New research has shown that previous periods of hotter temperatures were regionally based - covering no more than 40 per cent of the planet's surface.",
          "Date": "24 July 2019",
          "Facebook": "Daily Mail",
          "ArticleURL": "https:\u002F\u002Fwww.dailymail.co.uk\u002Fsciencetech\u002Farticle-7281697\u002FGlobal-warming-happening-speed-scale-unprecedented-2-000-years.html",
          "WebsiteURL": "dailymail.co.uk",
          "FacebookURL": "DailyMail",
          "FacebookType": "Media\u002Fnews company",
          "WikipediaURL": "Daily_Mail",
          "WikipediaCaption": "The Daily Mail is a British daily middle-market newspaper published in London in a tabloid format. Founded in 1896, it is the United Kingdom's highest-circulated daily newspaper. Its sister paper The Mail on Sunday was launched in 1982, while Scottish and Irish editions of the daily paper were launched in 1947 and 2006 respectively. Content from the paper appears on the MailOnline website, although the website is managed separately and has its own editor.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "daily-mail.png",
          "Image": "https:\u002F\u002Fi.dailymail.co.uk\u002F1s\u002F2019\u002F07\u002F24\u002F17\u002F16452954-7281697-image-a-3_1563986485600.jpg"
        },
        {
          "Name": "Letter",
          "Title": "There is no climate emergency, say 500 experts in letter to the United Nations",
          "Subtitle": "The video above is from Friends of Science, a Canada-based “non-profit organization run by dedicated volunteers comprised mainly of active and retired earth and atmospheric scientists, engineers, and other professionals.”",
          "Caption": "A group of 500 prominent scientists and professionals sent this registered letter to the United Nations Secretary-General stating that there is no climate emergency and climate policies should be designed to benefit the lives of people.",
          "Date": "01 October 2019",
          "Facebook": "American Enterprise Institute",
          "ArticleURL": "https:\u002F\u002Fwww.aei.org\u002Fcarpe-diem\u002Fthere-is-no-climate-emergency-say-500-experts-in-letter-to-the-united-nations\u002F",
          "WebsiteURL": "aei.org",
          "FacebookURL": "aei",
          "FacebookType": "Non-profit organisation",
          "WikipediaURL": "American_Enterprise_Institute",
          "WikipediaCaption": "The American Enterprise Institute for Public Policy Research, known simply as the American Enterprise Institute (AEI), is a Washington, D.C.–based think tank that researches government, politics, economics, and social welfare. AEI is an independent nonprofit organization supported primarily by grants and contributions from foundations, corporations, and individuals. Founded in 1938, AEI is commonly associated with conservatism and neoconservatism, although it is officially non-partisan in that it does not support a political party.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "aei.png",
          "Image": "https:\u002F\u002Fwww.studentnewsdaily.com\u002Fwp-content\u002Fuploads\u002F2019\u002F10\u002F500-Scientists-tell-UN-there-is-no-climate-emergency.png"
        },
        {
          "Name": "Crops",
          "Title": "GMO corn is safe and healthier than unmodified varieties says Italian study",
          "Subtitle": "Are GMO crops less healthy than unmodified varieties? A group of Italian scientists decided to find out for themselves, conducting a meta-analysis of peer-reviewed studies on genetically engineered corn.",
          "Caption": "The results of the study show that the GMO varieties have definite advantages over their nonmodified brethren.",
          "Date": "05 March 2018",
          "Facebook": "Duluth News Tribune",
          "ArticleURL": "https:\u002F\u002Fwww.duluthnewstribune.com\u002Fbusiness\u002Fagriculture\u002F4412121-gmo-corn-safe-and-healthier-unmodified-varieties-says-italian-study",
          "WebsiteURL": "duluthnewstribune.com",
          "FacebookURL": "duluthnews",
          "FacebookType": "Broadcasting & media production company",
          "WikipediaURL": "Duluth_News_Tribune",
          "WikipediaCaption": "The Duluth News Tribune is a newspaper based in Duluth, Minnesota. While circulation is heaviest in the Twin Ports metropolitan area, delivery extends into northeastern Minnesota, northwestern Wisconsin, and Michigan's Upper Peninsula. The paper has a limited distribution in Thunder Bay, Ontario. The News Tribune has been owned by Forum Communications since 2006.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "duluth-news-tribune.png",
          "Image": "https:\u002F\u002Fwww.fccnn.com\u002Fbusiness\u002Farticle810227.ece\u002Falternates\u002FBASE_LANDSCAPE\u002FA%20new%20study%20finds%20that%20farmers%20who%20plant%20Bt%20corn%20reduce%20crop%20damage%20and%20insecticide%20use%20in%20nearby%20fields%20of%20non-genetically%20engineered%20crops%20such%20as%20peppers%20and%20green%20beans.%20Erin%20Brown%20%20Grand%20Vale%20Creative"
        },
        {
          "Name": "Kidney",
          "Title": "Opting for fruit, vegetables and fish over processed meats, salt and fizzy drinks 'may slash your risk of getting kidney disease by 30%'",
          "Subtitle": "Following a healthy diet may reduce your risk of suffering kidney disease, research suggests.",
          "Caption": "Scientists at Bond University in Australia analysed the dietary habits of more than 630,000 people over a decade. They found those who opted for fruit, vegetables and fish over processed meats, salt and fizzy drinks were 30 per cent less likely to develop chronic kidney disease (CKD).",
          "Date": "24 September 2019",
          "Facebook": "Daily Mail",
          "ArticleURL": "https:\u002F\u002Fwww.dailymail.co.uk\u002Fhealth\u002Farticle-7498663\u002FEating-fruit-vegetables-fish-slashes-persons-risk-chronic-kidney-disease-30.html",
          "WebsiteURL": "dailymail.co.uk",
          "FacebookURL": "DailyMail",
          "FacebookType": "Media\u002Fnews company",
          "WikipediaURL": "Daily_Mail",
          "WikipediaCaption": "The Daily Mail is a British daily middle-market newspaper published in London in a tabloid format. Founded in 1896, it is the United Kingdom's highest-circulated daily newspaper. Its sister paper The Mail on Sunday was launched in 1982, while Scottish and Irish editions of the daily paper were launched in 1947 and 2006 respectively. Content from the paper appears on the MailOnline website, although the website is managed separately and has its own editor.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "daily-mail.png",
          "Image": "https:\u002F\u002Fi.dailymail.co.uk\u002F1s\u002F2019\u002F09\u002F24\u002F13\u002F18867990-7498663-image-a-78_1569327442159.jpg"
        },
        {
          "Name": "Chocolate",
          "Title": "Eating chocolate can help you LOSE weight, shock study discovers",
          "Subtitle": "GOOD news slimmers – new research claims that eating chocolate can actually help you beat the bulge",
          "Caption": "A German study has found that eating chocolate can reduce your waistline, lower your cholesterol and help you sleep.",
          "Date": "07 July 2018",
          "Facebook": "Daily Star",
          "ArticleURL": "https:\u002F\u002Fwww.dailystar.co.uk\u002Fdiet-fitness\u002Fchocolate-diet-how-lose-weight-16836113",
          "WebsiteURL": "dailystar.co.uk",
          "FacebookURL": "thedailystar",
          "FacebookType": "Media\u002Fnews company",
          "WikipediaURL": "Daily_Star_(United_Kingdom)",
          "WikipediaCaption": "The Daily Star is a daily tabloid newspaper published from Monday to Saturday in the United Kingdom since 2 November 1978. On 15 September 2002 a sister Sunday edition, Daily Star Sunday was launched with a separate staff. On 31 October 2009, the Daily Star published its 10,000th issue. Jon Clark is the editor-in-chief of the paper.",
          "Policies": "Corrections and Editorial team",
          "Registered": "More than 10 years ago",
          "Logo": "daily-star.png",
          "Image": "https:\u002F\u002Fi2-prod.dailystar.co.uk\u002Fincoming\u002Farticle15155403.ece\u002FALTERNATES\u002Fs615b\u002F0_Chocolate-weight-loss-433688"
        },
        {
          "Name": "2000B",
          "Title": "Climate change: Current warming 'unparalleled' in 2,000 years",
          "Subtitle": "The speed and extent of current global warming exceeds any similar event in the past 2,000 years, researchers say.",
          "Caption": "The research suggests that the current warming rate is higher than any observed previously.",
          "Date": "24 July 2019",
          "Facebook": "BBC News",
          "ArticleURL": "https:\u002F\u002Fwww.bbc.com\u002Fnews\u002Fscience-environment-49086783",
          "WebsiteURL": "bbc.co.uk",
          "FacebookURL": "bbcnews",
          "FacebookType": "Media\u002Fnews company",
          "WikipediaURL": "BBC_News_Online",
          "WikipediaCaption": "BBC News Online is the website of BBC News, the division of the BBC responsible for newsgathering and production.",
          "Policies": "Ownership and funding, Fact-checking, Corrections and Ethics",
          "Registered": "More than 10 years ago",
          "Logo": "bbcnews.png",
          "Image": "https:\u002F\u002Fichef.bbci.co.uk\u002Fnews\u002F976\u002Fcpsprodpb\u002F1843\u002Fproduction\u002F_108011260_gettyimages-172485870.jpg"
        },
        {
          "Name": "Summit",
          "Title": "Rain falls on Greenland’s snowy summit for the first time on record",
          "Subtitle": "Another worrying sign of the climate crisis",
          "Caption": "Precipitation at the summit of Greenland, about three kilometers above the sea level, fell as rain and not as snow for the first time since record-keeping began in 1950. Temperatures at the summit rose above freezing for the third time in a decade, signaling severe trouble for Greenland.",
          "Date": "24 August 2021",
          "Facebook": "ZME Science",
          "ArticleURL": "https:\u002F\u002Fwww.zmescience.com\u002Fecology\u002Frain-falls-on-greenlands-snowy-summit-for-the-first-time-on-record\u002F",
          "WebsiteURL": "zmescience.com",
          "FacebookURL": "zmescience",
          "FacebookType": "Science website",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "zmescience.png",
          "Image": "https:\u002F\u002Fcdn.zmescience.com\u002Fwp-content\u002Fuploads\u002F2021\u002F08\u002F25623219672_09db19f236_b.jpg"
        },
        {
          "Name": "LetterB",
          "Title": "There is no climate emergency",
          "Subtitle": "Five hundred scientists signed a letter to the United Nations saying there is no climate crisis!",
          "Caption": "The world has warmed at less than half the originally-predicted rate, and at less than half the rate to be expected on the basis of net anthropogenic forcing and radiative imbalance. It tells us that we are far from understanding climate change.",
          "Date": "23 September 2019",
          "Facebook": "Heartland Institute",
          "ArticleURL": "https:\u002F\u002Fwww.heartland.org\u002Fpublications-resources\u002Fpublications\u002Fthere-is-no-climate-crisis",
          "WebsiteURL": "heartland.org",
          "FacebookURL": "HeartlandInstitute",
          "FacebookType": "Non-profit organisation",
          "WikipediaURL": "Heartland_Institute",
          "WikipediaCaption": "The Heartland Institute is an American conservative and libertarian public policy think tank known for its rejection of the scientific consensus on climate change and the negative health impacts of smoking.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "heartland.jpg",
          "Image": "https:\u002F\u002Fwww.heartland.org\u002Fsebin\u002Fb\u002Fo\u002Fchild-enjoying-sun-right-size.jpg"
        },
        {
          "Name": "Cannabis",
          "Title": "Recent cannabis use linked to heart attack risk in younger adults",
          "Subtitle": "This observational study provides information on the relationship, but not biological mechanism, for cannabis use and myocardial infarction.",
          "Caption": "Adults younger than 45 years who reported recently using cannabis were 2 times more likely to have had a heart attack (myocardial infarction), and this link was stronger in frequent users.",
          "Date": "07 September 2021",
          "Facebook": "Medical Xpress - Medical and Health News",
          "ArticleURL": "https:\u002F\u002Fmedicalxpress.com\u002Fnews\u002F2021-09-cannabis-linked-heart-younger-adults.html",
          "WebsiteURL": "medicalxpress.com",
          "FacebookURL": "medicalxpress",
          "FacebookType": "Medical & health",
          "WikipediaURL": "Phys.org",
          "WikipediaCaption": "Phys.org is a UK-based science, research and technology news aggregator offering briefs from press releases and news agencies. It also summarizes journal reports and produces its own science journalism.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "medical-xpress.png",
          "Image": "https:\u002F\u002Fmedx.b-cdn.net\u002Ftmpl\u002Fv6\u002Fimg\u002Flogo.png"
        },
        {
          "Name": "CannabisB",
          "Title": "Recent pot use linked to increased heart attack risk in young adults",
          "Subtitle": "Eighteen- to 44-year-olds who used pot were twice as likely to have a heart attack compared with non-users, whether they smoked, vaped or ate their weed, researchers found.",
          "Caption": "Marijuana has been linked to a doubling in the risk of a heart attack in younger adults, no matter how they use it, a new study reports.",
          "Date": "07 September 2021",
          "Facebook": "UPI News Agency",
          "ArticleURL": "https:\u002F\u002Fwww.upi.com\u002FHealth_News\u002F2021\u002F09\u002F07\u002Fmarijuana-use-heart-attack-risk-study\u002F3141631020454\u002F",
          "WebsiteURL": "upi.com",
          "FacebookURL": "UPI",
          "FacebookType": "Media\u002Fnews company",
          "WikipediaURL": "United_Press_International",
          "WikipediaCaption": "United Press International (UPI) is an American international news agency whose newswires, photo, news film, and audio services provided news material to thousands of newspapers, magazines, radio and television stations for most of the 20th century.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "upi.jpg",
          "Image": "https:\u002F\u002Fcdnph.upi.com\u002Fsvc\u002Fsv\u002Fupi_com\u002F3141631020454\u002F2021\u002F1\u002F5aca1225e48e57bbf74912889f0b10e7\u002FRecent-pot-use-linked-to-increased-heart-attack-risk-in-young-adults.jpg"
        },
        {
          "Name": "Unsettled",
          "Title": "According to Obama science director, climate science is \"unsettled\"",
          "Subtitle": "As I write, in just over 12 hours since its official launch on May 4, Unsettled: What Climate Science Tells Us, What it Doesn’t, and Why It Matters, by physicist Steven E. Koonin, Ph.D., is number 15 on Amazon’s list of top-selling nonfiction books",
          "Caption": "Heat waves in the US are now no more common than they were in 1900 and the warmest temperatures in the US have not risen in the past fifty years.",
          "Date": "06 May 2021",
          "Facebook": "Heartland Institute",
          "ArticleURL": "https:\u002F\u002Fwww.heartland.org\u002Fnews-opinion\u002Fnews\u002Fclimate-science-is-unsettled-says-obama-science-director",
          "WebsiteURL": "heartland.org",
          "FacebookURL": "HeartlandInstitute",
          "FacebookType": "Non-profit organisation",
          "WikipediaURL": "Heartland_Institute",
          "WikipediaCaption": "The Heartland Institute is an American conservative and libertarian public policy think tank known for its rejection of the scientific consensus on climate change and the negative health impacts of smoking.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "heartland.jpg",
          "Image": "https:\u002F\u002Fwww.heartland.org\u002Fsebin\u002Fp\u002Fi\u002FCCW-396.png"
        },
        {
          "Name": "Deniers",
          "Title": "Only 3% of the 3% of climate change deniers are actually qualified in climate science",
          "Subtitle": "Climate change deniers are only good for blowing smoke.",
          "Caption": "Out of the 3% of scientists who don’t agree on anthropogenic global warming, the overwhelming majority aren’t qualified to assess it.",
          "Date": "17 December 2021",
          "Facebook": "ZME Science",
          "ArticleURL": "https:\u002F\u002Fwww.zmescience.com\u002Fecology\u002Fclimate\u002Fat-least-97-of-experts-agree-that-climate-change-is-real-and-caused-by-humans-but-only-3-of-the-3-of-climate-change-deniers-are-actually-qualified-in-climate-science\u002F",
          "WebsiteURL": "zmescience.com",
          "FacebookURL": "zmescience",
          "FacebookType": "Science website",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "zmescience.png",
          "Image": "https:\u002F\u002Fcdn.zmescience.com\u002Fwp-content\u002Fuploads\u002F2021\u002F12\u002Fair-pollution-g7c283fe31_1280-1024x594.jpg"
        },
        {
          "Name": "Smoking",
          "Title": "First day of attempting to quit smoking is especially tough for women",
          "Subtitle": "This is a particularly significant finding as the first day of abstinence is one of the most critical predictors of long-term smoking cessation.",
          "Caption": "A study of smokers found that the first day of a quit attempt is more challenging for women than men in 12 low- and middle-income countries, where around 60 percent of the world's smokers live.",
          "Date": "10 January 2022",
          "Facebook": "Medical Xpress - Medical and Health News",
          "ArticleURL": "https:\u002F\u002Fmedicalxpress.com\u002Fnews\u002F2022-01-day-tough-women.html",
          "WebsiteURL": "medicalxpress.com",
          "FacebookURL": "medicalxpress",
          "FacebookType": "Medical & health",
          "WikipediaURL": "Phys.org",
          "WikipediaCaption": "Phys.org is a UK-based science, research and technology news aggregator offering briefs from press releases and news agencies. It also summarizes journal reports and produces its own science journalism.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "medical-xpress.png",
          "Image": "https:\u002F\u002Fscx2.b-cdn.net\u002Fgfx\u002Fnews\u002Fhires\u002F2021\u002Fsmoking.jpg"
        },
        {
          "Name": "Equal",
          "Title": "Equal Warming, 1900 to 1950 versus 1950 to 2018: Why the UN Knows the First Half was Natural",
          "Subtitle": "Mathematics and Statistics Professor Caleb Stewart Rossiter Helps You “Do the Math” of Logarithms",
          "Caption": "Recorded temperature rose the same amount from 1950 to 2018 as it did from 1900 to 1950, and rates of extreme weather and sea-level rise were also the same in both periods. The data to date do not support claims of a CO2-caused “climate crisis.”",
          "Date": "09 March 2020",
          "Facebook": "CO2 Coalition",
          "ArticleURL": "https:\u002F\u002Fco2coalition.org\u002Fnews\u002Fequal-warming-1900-to-1950-versus-1950-to-2018-why-the-un-knows-the-first-half-was-natural\u002F",
          "WebsiteURL": "co2coalition.org",
          "FacebookURL": "TheCO2Coalition",
          "FacebookType": "Charitable organisation",
          "WikipediaURL": "CO2_Coalition",
          "WikipediaCaption": "The CO2 Coalition is a nonprofit advocacy organization in the United States founded in 2015. The group's claims are disputed by the vast majority of climate scientists. The organization has 69 members. The organization been funded by energy industry firms and conservative activists who oppose climate change mitigation policies, such as the Mercer Family Foundation and Koch brothers. It is viewed as the successor to the George C. Marshall Institute.",
          "Policies": "FALSE",
          "Registered": "More than 5 years ago",
          "Logo": "CO2_coalition.png",
          "Image": "https:\u002F\u002Fco2coalition.org\u002Fwp-content\u002Fuploads\u002F2021\u002F11\u002Fog-img.png"
        },
        {
          "Name": "Wall",
          "Title": "‘A Supermassive Wall’ Beneath The Earth Ocean Encompasses The Entire Planet",
          "Subtitle": "A mysterious video posted on YouTube claims there is a supermassive wall located beneath Earth’s oceans, encompassing the entire planet.",
          "Caption": "According to many, this wall’s sheer size and its adherence to linearity indicate it is not a natural formation. In fact, many people are convinced that due to the numerous discoveries made across the globe which completely contradict history as we have been taught in school, something like this is entirely possible.",
          "Date": "01 November 2020",
          "Facebook": "ScienceVibe",
          "ArticleURL": "https:\u002F\u002Fsciencevibe.com\u002F2020\u002F11\u002F01\u002Fmysterious-underwater-wall-that-circles-the-entire-planet-found-on-google-earth-video-2\u002F",
          "WebsiteURL": "sciencevibe.com",
          "FacebookURL": "sciencevibe",
          "FacebookType": "Science website",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 5 years ago",
          "Logo": "sciencevibe.jpg",
          "Image": "https:\u002F\u002Fsciencevibe.com\u002Fwp-content\u002Fuploads\u002F2018\u002F04\u002Fgoogle-earth-giant-underwater-wall-encompasses-our-entire-planet-138378-800x445.jpg"
        },
        {
          "Name": "Tigers",
          "Title": "Despite World-Wide Threat, Indian Tigers Are Making A Comeback!",
          "Subtitle": "The story of tigers is a sad one. In the beginning of the 20th century, they numbered 100,000, but now they have dwindled to a mere 3,000. But there is one bright spot – India, where concerned conservation efforts have had striking effects.",
          "Caption": "The number of tigers  in the country, have increased to 2,226 from 1,411 seven years ago, a rise of nearly 58 percent. India is now home to about 70 percent of the world’s wild tigers.",
          "Date": "02 January 2021",
          "Facebook": "ScienceVibe",
          "ArticleURL": "https:\u002F\u002Fsciencevibe.com\u002F2021\u002F01\u002F02\u002Fdespite-world-wide-threat-indian-tigers-are-making-a-comeback\u002F",
          "WebsiteURL": "sciencevibe.com",
          "FacebookURL": "sciencevibe",
          "FacebookType": "Science website",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 5 years ago",
          "Logo": "sciencevibe.jpg",
          "Image": "https:\u002F\u002Fsciencevibe.com\u002Fwp-content\u002Fuploads\u002F2015\u002F01\u002F1414407352S%C3%B8dtiger.jpg"
        },
        {
          "Name": "Arctic",
          "Title": "Arctic Ocean started to warm decades earlier than scientists thought",
          "Subtitle": "Researchers reconstructed the recent history of ocean warming near the Arctic Ocean in a region called the Fram Strait, between Greenland and Svalbard, finding that the Arctic has been warming for much longer than earlier records suggested.",
          "Caption": "The Arctic Ocean has been warming since the beginning of the 20th century, fueled by a process known as Atlantification.",
          "Date": "24 November 2021",
          "Facebook": "UPI News Agency",
          "ArticleURL": "https:\u002F\u002Fwww.upi.com\u002FScience_News\u002F2021\u002F11\u002F24\u002Farctic-warming-atlantification\u002F2141637764455",
          "WebsiteURL": "upi.com",
          "FacebookURL": "UPI",
          "FacebookType": "Media\u002Fnews company",
          "WikipediaURL": "United_Press_International",
          "WikipediaCaption": "United Press International (UPI) is an American international news agency whose newswires, photo, news film, and audio services provided news material to thousands of newspapers, magazines, radio and television stations for most of the 20th century.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "upi.jpg",
          "Image": "https:\u002F\u002Fcdnph.upi.com\u002Fsvc\u002Fsv\u002Fi\u002F2141637764455\u002F2021\u002F1\u002F16377807759109\u002FArctic-Ocean-started-to-warm-decades-earlier-than-scientists-thought.jpg"
        },
        {
          "Name": "Pacific",
          "Title": "The great pacific garbage patch twice the size of texas is fake",
          "Subtitle": "There is a large distinction between pollution in the sea, and litter, the latter of which is often not harmful to the environment and can actually be beneficial to ecological systems.",
          "Caption": "Plastic litter may appear unsightly, but like driftwood on a beach it is not toxic and does not harm life. Like driftwood in the ocean, plastic promotes life, as many marine species attach themselves to it, lay their eggs on it, or eat other species that are living on it. Floating pieces of plastic are like small floating reefs that enhance rather that harm marine life.",
          "Date": "19 November 2021",
          "Facebook": "Heartland Institute",
          "ArticleURL": "https:\u002F\u002Fwww.heartland.org\u002Fnews-opinion\u002Fnews\u002Fthe-great-pacific-garbage-patch-twice-the-size-of-texas-is-fake",
          "WebsiteURL": "heartland.org",
          "FacebookURL": "HeartlandInstitute",
          "FacebookType": "Non-profit organisation",
          "WikipediaURL": "Heartland_Institute",
          "WikipediaCaption": "The Heartland Institute is an American conservative and libertarian public policy think tank known for its rejection of the scientific consensus on climate change and the negative health impacts of smoking.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "heartland.jpg",
          "Image": "https:\u002F\u002Fwww.heartland.org\u002Fsebin\u002Fx\u002Fs\u002Flitter%20crop_2_pyramid.jpg"
        },
        {
          "Name": "Cigars",
          "Title": "FDA study: cancer risks nearly nil for 1-2 cigars per day",
          "Subtitle": "Smoking up to two cigars a day is associated with minimal significant health risks.",
          "Caption": "While not completely safe, consumption of up to two cigars per day is not associated with significantly increased risks for smoking-related cancers.",
          "Date": "25 August 2016",
          "Facebook": "Heartland Institute",
          "ArticleURL": "https:\u002F\u002Fwww.heartland.org\u002Fnews-opinion\u002Fnews\u002Ffda-study-cancer-risks-nearly-nil-for-1-2-cigars-per-day",
          "WebsiteURL": "heartland.org",
          "FacebookURL": "HeartlandInstitute",
          "FacebookType": "Non-profit organisation",
          "WikipediaURL": "Heartland_Institute",
          "WikipediaCaption": "The Heartland Institute is an American conservative and libertarian public policy think tank known for its rejection of the scientific consensus on climate change and the negative health impacts of smoking.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "heartland.jpg",
          "Image": "https:\u002F\u002Fwww.heartland.org\u002Fsebin\u002Fv\u002Fl\u002Fcigars_2_pyramid.jpg"
        },
        {
          "Name": "Sponges",
          "Title": "Researchers: Microwave oven can sterilize sponges, scrub pads",
          "Subtitle": "Microwave ovens may be good for more than just zapping the leftovers; they may also help protect your family.",
          "Caption": "To decontaminate sponges and plastic scrubbers – known to be common carriers of the bacteria and viruses that cause food-borne illnesses – people could use the microwave, which knocks out most bacteria in two minutes.",
          "Date": "22 January 2007",
          "Facebook": "Medical Xpress - Medical and Health News",
          "ArticleURL": "https:\u002F\u002Fmedicalxpress.com\u002Fnews\u002F2007-01-microwave-oven-sterilize-sponges-pads.html",
          "WebsiteURL": "medicalxpress.com",
          "FacebookURL": "medicalxpress",
          "FacebookType": "Medical & health",
          "WikipediaURL": "Phys.org",
          "WikipediaCaption": "Phys.org is a UK-based science, research and technology news aggregator offering briefs from press releases and news agencies. It also summarizes journal reports and produces its own science journalism.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "medical-xpress.png",
          "Image": "https:\u002F\u002Fmedx.b-cdn.net\u002Ftmpl\u002Fv6\u002Fimg\u002Flogo.png"
        },
        {
          "Name": "Snowfall",
          "Title": "Snowfall in the Alps is full of plastics particles",
          "Subtitle": "This study says plastic pollution in the air is way worse than we imagined.",
          "Caption": "The findings suggest that around 3,000 tons of nanoplastic particles are deposited in Switzerland every year, including the most remote Alpine regions.",
          "Date": "26 January 2022",
          "Facebook": "ZME Science",
          "ArticleURL": "https:\u002F\u002Fwww.zmescience.com\u002Fscience\u002Falpine-snow-plastic-pollution-93737553\u002F",
          "WebsiteURL": "zmescience.com",
          "FacebookURL": "zmescience",
          "FacebookType": "Science website",
          "WikipediaURL": "FALSE",
          "WikipediaCaption": "FALSE",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "zmescience.png",
          "Image": "https:\u002F\u002Fcdn.zmescience.com\u002Fwp-content\u002Fuploads\u002F2022\u002F01\u002F26878989221_a6d4df66b6_k.jpg"
        },
        {
          "Name": "Microchip",
          "Title": "Swedish company creates under-the-skin microchip to carry covid-19 passports in user's arms",
          "Subtitle": "Stockholm: Dystopian nightmare or a simple convenience? A Swedish company implanting microchips under the skin has is promoting its devices for use as a COVID-19 health pass in a country with thousands of early adopters.",
          "Caption": "For the CEO of the Swedish company, the Covid pass is just one example of a possible application, which will be a \"thing for the winter of 2021-2022\". While he acknowledged that many \"people see chip implants as a scary technology, as a surveillance technology\", Sjoblad said that instead they should be viewed as a simple ID tag.",
          "Date": "23 December 2021",
          "Facebook": "Firstpost",
          "ArticleURL": "https:\u002F\u002Fwww.firstpost.com\u002Ftech\u002Fnews-analysis\u002Fswedish-company-creates-under-the-skin-microchip-to-carry-covid-19-passports-in-users-arms-10230701.html",
          "WebsiteURL": "firstpost.com",
          "FacebookURL": "firstpostin",
          "FacebookType": "News & media website",
          "WikipediaURL": "Firstpost",
          "WikipediaCaption": "Firstpost is an Indian news and media website. The site is a part of the Network 18 media conglomerate owned by Reliance Industries, which also runs CNN-News18 and CNBC-TV18.",
          "Policies": "FALSE",
          "Registered": "More than 10 years ago",
          "Logo": "firstpost.jpg",
          "Image": "https:\u002F\u002Fimages.firstpost.com\u002Fwp-content\u002Fuploads\u002F2021\u002F12\u002F23pass.jpg"
        }
      ],
      "sample": {
        "mode": "draw-shuffle"
      },
      "files": {},
      "responses": {
        "": ""
      },
      "parameters": {},
      "messageHandlers": {
        "before:prepare": function anonymous(
) {
if (typeof this.state.url["POST"] === 'undefined'){
  this.state.url["POST"] = 'Cigars';
}

if (typeof this.state.url["INFO"] === 'undefined'){
  this.state.url["INFO"] = 'popup';
}

// keep only one post
this.options.templateParameters = this.options.templateParameters.filter(post => post.Name === this.state.url["POST"]);

// define experimental condition
this.options.parameters.condition = this.state.url["INFO"];

// message event handler 
window.addEventListener('message', function qualtrics_data_handler(event) {
  if (event.data === 'submit') {
    var screen = study.internals.currentComponent.internals.currentComponent;
    screen.end(); //.internals.currentComponent
  }
}, false);

}
      },
      "title": "Articles",
      "tardy": true,
      "shuffleGroups": [],
      "template": {
        "type": "lab.html.Screen",
        "files": {
          "picture1.png": "embedded\u002F025e4334a312f0762228cc8adcd6835619e297161181534539d8a22fccb43fa1.png",
          "picture2.png": "embedded\u002F669a03fd7f4f9ebb0c8c2a8dfc9cca1a3648ab65f8c1a8fc92419b50c8a40bde.png",
          "picture3.png": "embedded\u002F49c2b0629dace0f0d7805d7bfb140e4d9054fef038fd01867bf61f48ca8ecc6f.png",
          "logo.svg": "embedded\u002F5b216480b0cd5ca31a3ac7ac54c870a54d09c595a3c7ab3a2cce730bd9153ad3.svg"
        },
        "responses": {},
        "parameters": {},
        "messageHandlers": {
          "before:prepare": function anonymous(
) {
this.options.parameters.facebook   = [];
this.options.parameters.website    = [];
this.options.parameters.wikipedia  = [];
this.options.parameters.creative   = [];
this.options.parameters.whois      = [];
this.options.parameters.leave      = [];
this.options.parameters.comeback   = [];
this.options.parameters.info_open  = [];
this.options.parameters.info_close = [];
this.options.parameters.popup      = [];

this.options.events['mouseup a[name="fbpage"]'] = () => {
  this.options.parameters.facebook.push(new Date().toISOString()+'|'+this.timer);
}

this.options.events['mouseup a[name="articlepage"]'] = () => {
  this.options.parameters.website.push(new Date().toISOString()+'|'+this.timer);
}

this.options.events['mouseup a[name="wikipedia"]'] = () => {
  this.options.parameters.wikipedia.push(new Date().toISOString()+'|'+this.timer);
}

this.options.events['mouseup a[name="creative"]'] = () => {
  this.options.parameters.creative.push(new Date().toISOString()+'|'+this.timer);
}

this.options.events['mouseup a[name="whois"]'] = () => {
  this.options.parameters.whois.push(new Date().toISOString()+'|'+this.timer);
}

this.options.events['visibilitychange'] = () => {
  if (document.hidden){
    this.options.parameters.leave.push(new Date().toISOString()+'|'+this.timer);
  } else {    
    this.options.parameters.comeback.push(new Date().toISOString()+'|'+this.timer);
  }
}

// aria label for info button
this.options.events["mouseover div#info_button"] = () => {
  document.querySelector("div#info_aria").style.display = "block";
  }
this.options.events["mouseout div#info_button"] = () => {
  document.querySelector("div#info_aria").style.display = "none";
  }

this.options.events["mouseup div#contextbutton"] = () => {
  this.options.parameters.info_open.push(new Date().toISOString()+'|'+this.timer);
  document.querySelector("div#articlecontext").style.visibility = "visible";
  }

this.options.events["mouseup a#closearticlecontext"] = () => {
  this.options.parameters.info_close.push(new Date().toISOString()+'|'+this.timer);
  document.querySelector("div#articlecontext").style.visibility = "hidden";
  }

// click on background to close info window
this.options.events["mouseup div.i09qtzwb.n7fi1qx3.poy2od1o.j9ispegn.kr520xx4.ms7hmo2b"] = () => {
  this.options.parameters.info_close.push(new Date().toISOString()+'|'+this.timer);
  document.querySelector("div#articlecontext").style.visibility = "hidden";
  }

// Popup
this.options.events["mouseover div#popup_button"] = () => {
  document.querySelector("div#popup_close").style.opacity=1;
  }

this.options.events["mouseout div#popup_button"] = () => {
  document.querySelector("div#popup_close").style.opacity=0;
  }

this.options.events["mouseup div#popup_button"] = () => {
  this.options.parameters.popup.push(new Date().toISOString()+'|'+this.timer);
  document.querySelector("div#popup").style.display = "none";
  }
},
          "run": function anonymous(
) {
this.options.parameters.blur     = [];
this.options.parameters.focus    = [];

// check blur
document.body.onblur = () => {
  this.options.parameters['blur'].push(new Date().toISOString()+'|'+this.timer);
}

document.body.onfocus = () => {
  this.options.parameters['focus'].push(new Date().toISOString()+'|'+this.timer);
}
}
        },
        "title": "Post",
        "content": "\u003Cdiv id=\"popup\" style=\"display:${state.url['INFO'] == 'popup' ? 'inherit' : 'none'};position:absolute; z-index:1000;\" class=\"gs1a9yip rq0escxv j83agx80 cbu4d94t taijpn5t cou93tnk dflh9lhu scb9dxdr ir0402vp n7vda9r4\"\u003E\r\n  \u003Cdiv style=\"background-image:linear-gradient(rgba(96,103,112,.8),rgba(96,103,112,.9),rgba(96,103,112,1))\" class=\"i09qtzwb n7fi1qx3 poy2od1o j9ispegn kr520xx4\"\u003E\u003C\u002Fdiv\u003E\r\n  \u003Cdiv class=\"ll8tlv6m j83agx80 taijpn5t hzruof5a\" style=\"min-height: 0px;\"\u003E\r\n      \u003Cdiv aria-label=\"Run a search engine\" class=\"j83agx80 cbu4d94t lzcic4wl ni8dbmo4 stjgntxs oqq733wu l9j0dhe7 du4w35lb cwj9ozl2 ue3kfks5 pw54ja7n uo3d90p7 l82x9zwi nwpbqux9 gc7gaz0o k4urcfbm\" role=\"dialog\"\u003E\r\n          \u003Cdiv class=\"linmgsc8 rq0escxv cb02d2ww clqubjjj bjjun2dj\"\u003E\r\n              \u003Cdiv class=\"bp9cbjyn rq0escxv j83agx80 datstx6m hv4rvrfc dati1w0a taijpn5t\"\u003E\r\n                  \u003Ch2 class=\"gmql0nx0 l94mrbxd p1ri9a11 lzcic4wl d2edcug0 hpfvmrgz\" dir=\"auto\" tabindex=\"-1\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 ns63r2gh fe6kdd0r mau55g9w c8b282yb rwim8176 o3w64lxj b2s5l15y hnhda86s oo9gr5id\" dir=\"auto\"\u003E\u003Cspan class=\"a8c37x1j ni8dbmo4 stjgntxs l9j0dhe7 ltmttdrg g0qnabr5 r8blr3vg\" style=\"font-size: 1.1rem;\"\u003ESearch information on other websites\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fh2\u003E\r\n              \u003C\u002Fdiv\u003E\r\n          \u003C\u002Fdiv\u003E\r\n          \u003Cdiv id=\"popup_button\" class=\"cypi58rs pmk7jnqg fcg2cn6m tkr6xdv7\"\u003E\r\n              \u003Cdiv aria-label=\"Close\" class=\"oajrlxb2 tdjehn4e qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 j83agx80 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l bp9cbjyn s45kfl79 emlxlaya bkmhp75w spb7xbtv rt8b4zig n8ej3o3l agehan2d sk4xxmp2 taijpn5t tv7at329 thwo4zme\" role=\"button\" tabindex=\"0\"\u003E\u003Ci class=\"hu5pjgll m6k467ps sp_Fca1SKCX0pI sx_c3320a\"\u003E\u003C\u002Fi\u003E\r\n                  \u003Cdiv id=\"popup_close\" style=\"opacity: 0;\" class=\"s45kfl79 emlxlaya bkmhp75w spb7xbtv i09qtzwb n7fi1qx3 hzruof5a pmk7jnqg j9ispegn kr520xx4 c5ndavph art1omkt ot9fgl3s rnr61an3\" data-visualcompletion=\"ignore\"\u003E\u003C\u002Fdiv\u003E\r\n              \u003C\u002Fdiv\u003E\r\n          \u003C\u002Fdiv\u003E\r\n          \u003Cdiv class=\"pybr56ya \u002F*ofv0k9yr*\u002F\"\u003E\r\n              \u003Cdiv data-visualcompletion=\"ignore-dynamic\" style=\"padding-left: 8px; padding-right: 8px;\"\u003E\r\n                  \u003Cdiv class=\"ue3kfks5 pw54ja7n uo3d90p7 l82x9zwi a8c37x1j\"\u003E\r\n                      \u003Cdiv class=\"ow4ym5g4 auili1gw rq0escxv j83agx80 buofh1pr g5gj957u i1fnvgqd oygrvhab cxmmr5t8 hcukyx3x kvgmc6g5 nnctdnn4 hpfvmrgz qt6c0cv9 jb3vyjys l9j0dhe7 du4w35lb bp9cbjyn btwxx1t3 dflh9lhu scb9dxdr\"\u003E\r\n                          \u003Cdiv class=\"text-left\"\u003E\r\n                            \u003Ctable border=\"0\" class=\"table-plain\"\u003E\r\n                                \u003Ctr style=\"\"\u003E\r\n                                    \u003Ctd rowspan=\"5\" style=\"width: 250px; padding: 0 20px 0 0; border: 0;\"\u003E\r\n                                        \u003Cimg src=${ this.files[\"logo.svg\"] } style=\"width: 206px\"\u003E\r\n                                    \u003C\u002Ftd\u003E\r\n                                    \u003C!--td rowspan=\"3\" style=\"width: 6.25%\"\u003E\u003C\u002Ftd--\u003E\r\n                                    \u003Ctd colspan=\"2\" style=\"\u002F*height: 35%; *\u002Ffont-size:20px;\"\u003ERun a search engine:\u003C\u002Ftd\u003E\r\n                                \u003C\u002Ftr\u003E\r\n                                \u003Ctr style=\"height: 28%\"\u003E\r\n                                    \u003Ctd style=\"\u002F*width: 20%*\u002F\"\u003E\r\n                                        \u003Cimg src=${ this.files[\"picture1.png\"] } style=\"width: 35px\"\u003E\r\n                                    \u003C\u002Ftd\u003E\r\n                                    \u003Ctd style=\"width:45%;font-size:19px;\"\u003E\u003Cspan style=\"font-weight: bold;\"\u003ECompare\u003C\u002Fspan\u003E claims\u003C\u002Ftd\u003E\r\n                                \u003C\u002Ftr\u003E\r\n                                \u003Ctr style=\"height: 28%\"\u003E\r\n                                    \u003Ctd style=\"\"\u003E\r\n                                        \u003Cimg src=${ this.files[\"picture3.png\"] } style=\"width: 35px\"\u003E\r\n                                    \u003C\u002Ftd\u003E\r\n                                    \u003Ctd style=\"font-size:19px;\"\u003ECheck \u003Cspan style=\"font-weight: bold;\"\u003Esources\u003C\u002Fspan\u003E\u003C\u002Ftd\u003E\r\n                                \u003C\u002Ftr\u003E\r\n                                \u003Ctr style=\"height: 28%\"\u003E\r\n                                    \u003Ctd style=\"\"\u003E\r\n                                        \u003Cimg src=${ this.files[\"picture2.png\"] } style=\"width: 35px\"\u003E\r\n                                    \u003C\u002Ftd\u003E\r\n                                    \u003Ctd style=\"font-size:19px;\"\u003E\u003Cspan style=\"font-weight: bold;\"\u003EDon't stop\u003C\u002Fspan\u003E at first results\u003C\u002Ftd\u003E\r\n                                \u003C\u002Ftr\u003E\r\n                                \u003Ctr style=\"height: 6%\"\u003E\r\n                                    \u003Ctd colspan=\"2\"\u003E\r\n                                    \u003C\u002Ftd\u003E\r\n                                \u003C\u002Ftr\u003E\r\n                            \u003C\u002Ftable\u003E\r\n                        \u003C\u002Fdiv\u003E\r\n                      \u003C\u002Fdiv\u003E\r\n                  \u003C\u002Fdiv\u003E\r\n              \u003C\u002Fdiv\u003E\r\n          \u003C\u002Fdiv\u003E\r\n          \u003Cdiv class=\"bp9cbjyn j83agx80 datstx6m hv4rvrfc dati1w0a taijpn5t\" style=\"height:40px;\u002F* border-top: 1px solid var(--media-inner-border); *\u002Ffont-size: 0.85rem;\"\u003EClose this pop-up to see the post\u003C\u002Fdiv\u003E\r\n      \u003C\u002Fdiv\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003Cdiv id=\"articlecontext\" style=\"position:absolute; visibility: hidden;\"\u003E\r\n    \u003Cdiv\u003E\r\n        \u003Cdiv class=\"l9j0dhe7 tkr6xdv7\"\u003E\r\n            \u003Cdiv class=\"rq0escxv l9j0dhe7 du4w35lb\"\u003E\r\n                \u003Cdiv data-pagelet=\"root\" class=\"h3gjbzrl l9j0dhe7\"\u003E\r\n                    \u003Cdiv class=\"i09qtzwb n7fi1qx3 poy2od1o j9ispegn kr520xx4 ms7hmo2b\"\u003E\u003C\u002Fdiv\u003E\r\n                    \u003Cdiv class=\"\"\u003E\r\n                        \u003Cdiv class=\"iqfcb0g7 tojvnm2t a6sixzi8 k5wvi7nf q3lfd5jv pk4s997a bipmatt0 cebpdrjk qowsmv63 owwhemhu dp1hu0rb dhp61c6y l9j0dhe7 iyyx5f41 a8s20v7p\"\u003E\r\n                            \u003Cdiv class=\"gs1a9yip rq0escxv j83agx80 cbu4d94t taijpn5t h3gjbzrl dflh9lhu scb9dxdr ir0402vp n7vda9r4\"\u003E\r\n                                \u003Cdiv class=\"ll8tlv6m j83agx80 taijpn5t hzruof5a\" style=\"min-height: 0px;\"\u003E\r\n                                    \u003Cdiv class=\"j83agx80 cbu4d94t lzcic4wl ni8dbmo4 stjgntxs oqq733wu l9j0dhe7 du4w35lb cwj9ozl2 ue3kfks5 pw54ja7n uo3d90p7 l82x9zwi nwpbqux9\" role=\"dialog\"\u003E\r\n                                        \u003Cdiv class=\"_3qw\" style=\"opacity: 1; min-width: 555px; z-index: 400\"\u003E\r\n                                            \u003Cdiv style=\"width: 555px; margin: 0px;\"\u003E\r\n                                                \u003Cdiv\u003E\r\n                                                    \u003Cdiv\u003E\r\n                                                        \u003Cdiv\u003E\r\n                                                            \u003Cdiv class=\"_4-i0 _9l16\"\u003E\r\n                                                                \u003Cdiv class=\"clearfix\"\u003E\r\n                                                                    \u003Cdiv class=\"_51-u rfloat _ohf\"\u003E\u003Ca id=\"closearticlecontext\" role=\"button\" class=\"_42ft _5upp _50zy layerCancel _51-t _9l15 _50-0 _50z-\" data-testid=\"dialog_title_close_button\" title=\"Close\"\u003EClose\u003C\u002Fa\u003E\u003C\u002Fdiv\u003E\r\n                                                                    \u003Cdiv\u003E\r\n                                                                        \u003Ch3 id=\"u_0_0\" class=\"_52c9 _9l17\"\u003EAbout this content\u003C\u002Fh3\u003E\r\n                                                                    \u003C\u002Fdiv\u003E\r\n                                                                \u003C\u002Fdiv\u003E\r\n                                                            \u003C\u002Fdiv\u003E\r\n                                                            \u003Cdiv class=\"_4-i2 _50f4\"\u003E\r\n                                                                \u003Cdiv\u003E\r\n                                                                    \u003Cdiv class=\"_27d7\" id=\"u_0_1\"\u003E\r\n                                                                        \u003Cdiv\u003E\r\n                                                                            \u003Cdiv class=\"_2pi9 _2pio\"\u003E\r\n                                                                                \u003Cdiv class=\"_2pie\"\u003E\r\n                                                                                    \u003Cdiv\u003E\r\n                                                                                        \u003Cdiv class=\"clearfix _IntegrityContextDialogProfileSummaryBlock__rootWithSubtitle\"\u003E\u003Ca name=\"fbpage\" class=\"_7jo2 _2brj _3-91 _8o lfloat _ohe\" target=\"_blank\" aria-hidden=\"true\" href=${\"https:\u002F\u002Fwww.facebook.com\u002F\"+parameters.FacebookURL}\u003E \u003Cdiv class=\"_2cpc _2bsm\"\u003E\u003Cimg class=\"_s0 _4ooo _2bsn _tzw img\" style=\"border-radius:50%;overflow:hidden;border:initial\" src=${\"static\u002F\"+parameters.Logo} alt=\"\" aria-label=${parameters.WebsiteURL} role=\"img\"\u003E\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E\r\n                                                                                        \u003Cdiv class=\"_42ef\"\u003E\r\n                                                                                            \u003Cdiv class=\"_2bso\"\u003E\u003Ca  name=\"fbpage\" class=\"_7jo2\" href=${\"https:\u002F\u002Fwww.facebook.com\u002F\"+parameters.FacebookURL} target=\"_blank\"\u003E\u003Cspan class=\"_2iel _50f7\"\u003E${parameters.Facebook}\u003C\u002Fspan\u003E\r\n                                                                                                    \u003C!--span\u003E \u003Ci class=\"img sp_YQP1I8yDIZ8 sx_84480b\"\u003E\u003C\u002Fi\u003E\u003C\u002Fspan--\u003E\u003C\u002Fa\u003E\r\n                                                                                                \u003Cdiv\u003E\u003Cspan class=\"_50f8 _50f4\"\u003E${parameters.FacebookType}\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                            \u003C\u002Fdiv\u003E\r\n                                                                                        \u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fdiv\u003E\r\n                                                                            \u003C\u002Fdiv\u003E\r\n                                                                            \u003Cdiv class=\"_2pie ${(parameters.WikipediaURL === \"FALSE\") ? 'hidden' : '' }\"\u003E\r\n                                                                                \u003Cdiv\u003E\r\n                                                                                    \u003Cdiv class=\"_6dm5 _2ien\"\u003E\u003Cspan\u003E${parameters.WikipediaCaption}\u003C\u002Fspan\u003E\u003Cspan class=\"_6dmn\"\u003E\u003Ca name=\"wikipedia\" class=\"_6dmo\" href=${\"https:\u002F\u002Fen.wikipedia.org\u002Fwiki\u002F\"+parameters.WikipediaURL} aria-label=\"Continue reading\" target=\"_blank\"\u003E\u003C\u002Fa\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                    \u003Cdiv class=\"_3-8w\"\u003E\r\n                                                                                        \u003Cdiv class=\"_6dmm _50f8 _50f3\"\u003EFrom \u003Ca name=\"wikipedia\" href=${\"https:\u002F\u002Fen.wikipedia.org\u002Fwiki\u002F\"+parameters.WikipediaURL} target=\"_blank\" data-ft=\"{&quot;tn&quot;:&quot;-U&quot;}\" rel=\"noopener nofollow\" data-lynx-mode=\"asynclazy\"\u003EWikipedia\u003C\u002Fa\u003E (\u003Ca name=\"creative\" href=\"https:\u002F\u002Fcreativecommons.org\u002Flicenses\u002Fby-sa\u002F3.0\u002F\" target=\"_blank\" data-ft=\"{&quot;tn&quot;:&quot;-U&quot;}\" rel=\"noopener nofollow\" data-lynx-mode=\"asynclazy\"\u003ECreative Commons\u003C\u002Fa\u003E)\u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fdiv\u003E\r\n                                                                            \u003C\u002Fdiv\u003E\r\n                                                                            \u003Cdiv class=\"_2pie\"\u003E\r\n                                                                                \u003Cdiv\u003E\r\n                                                                                    \u003Cdiv class=\"clearfix _ikh _2lul\"\u003E\r\n                                                                                        \u003Cdiv class=\"_4bl7\"\u003E\u003Ci class=\"_6akl _7jal _6akn\"\u003E\u003C\u002Fi\u003E\u003C\u002Fdiv\u003E\r\n                                                                                        \u003Cdiv class=\"_4bl9\"\u003E\r\n                                                                                            \u003Cdiv class=\"_2luk\"\u003E${parameters.WebsiteURL} registered: \u003Cstrong class=\"_6u9g\"\u003E${parameters.Registered}\u003C\u002Fstrong\u003E \u003Cspan class=\"_6tr5 _6u9g _50f8\"\u003EFrom \u003Ca name=\"whois\" href=\"https:\u002F\u002Fwhois.icann.org\u002F\" target=\"_blank\" rel=\"noopener nofollow\" data-lynx-mode=\"asynclazy\" data-lynx-uri=\"https:\u002F\u002Flookup.icann.org\u002F\"\u003EWHOIS\u003C\u002Fa\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                        \u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                    \u003Cdiv class=\"clearfix _ikh _2lul ${(parameters.Policies === \"FALSE\") ? 'hidden' : '' }\"\u003E\r\n                                                                                        \u003Cdiv class=\"_4bl7\"\u003E\u003Ci class=\"_6akl _7jal _7f6j\"\u003E\u003C\u002Fi\u003E\u003C\u002Fdiv\u003E\r\n                                                                                        \u003Cdiv class=\"_4bl9\"\u003E\r\n                                                                                            \u003Cdiv class=\"_2luk\"\u003E\r\n                                                                                                \u003Cdiv class=\"_7fp-\"\u003E\r\n                                                                                                    \u003Cdiv class=\"_7fp_\"\u003EPolicies and info: \u003Cstrong\u003E${parameters.Policies}\u003C\u002Fstrong\u003E\u003C\u002Fdiv\u003E\u003Ca href=\"#\" ajaxify=\"\u002Farticle_context\u002Ftrust_indicators_dialog\u002F?page_id=228735667216\" rel=\"dialog\" role=\"button\" class=\"_39g6\"\u003E\u003Ci class=\"img sp_kcd-eIOW5sF sx_77f0af\"\u003E\u003C\u002Fi\u003E\u003C\u002Fa\u003E\r\n                                                                                                \u003C\u002Fdiv\u003E \u003Cspan class=\"_6tr5 _6u9g _50f8\"\u003EFrom \u003Ca href=${\"https:\u002F\u002Fwww.facebook.com\u002F\"+parameters.FacebookURL} target=\"_blank\"\u003E${parameters.Facebook} \u003C\u002Fa\u003E \u003C\u002Fspan\u003E\r\n                                                                                            \u003C\u002Fdiv\u003E\r\n                                                                                        \u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fdiv\u003E\r\n                                                                            \u003C\u002Fdiv\u003E\r\n                                                                        \u003C\u002Fdiv\u003E\r\n                                                                    \u003C\u002Fdiv\u003E\r\n                                                                \u003C\u002Fdiv\u003E\r\n                                                            \u003C\u002Fdiv\u003E\r\n                                                        \u003C\u002Fdiv\u003E\r\n                                                    \u003C\u002Fdiv\u003E\r\n                                                \u003C\u002Fdiv\u003E\r\n                                            \u003C\u002Fdiv\u003E\r\n                                        \u003C\u002Fdiv\u003E\r\n                                    \u003C\u002Fdiv\u003E\r\n                                \u003C\u002Fdiv\u003E\r\n                            \u003C\u002Fdiv\u003E\r\n                        \u003C\u002Fdiv\u003E\r\n                    \u003C\u002Fdiv\u003E\r\n                \u003C\u002Fdiv\u003E\r\n            \u003C\u002Fdiv\u003E\r\n        \u003C\u002Fdiv\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003Cdiv name=\"post\" class=\"d2edcug0 oh7imozk tr9rh885 abvwweq7 ejjq64ki\"\u003E\r\n    \u003Cdiv\u003E\r\n        \u003Cdiv class=\"du4w35lb k4urcfbm l9j0dhe7 sjgh65i0\"\u003E\r\n            \u003Cdiv class=\"du4w35lb l9j0dhe7\"\u003E\r\n                \u003Cdiv class=\"\"\u003E\r\n                    \u003Cdiv class=\"\"\u003E\r\n                        \u003Cdiv aria-posinset=\"1\" aria-describedby=\"jsc_c_o jsc_c_p jsc_c_q jsc_c_s jsc_c_r\" aria-labelledby=\"jsc_c_n\" class=\"lzcic4wl\" role=\"article\"\u003E\r\n                            \u003Cdiv class=\"j83agx80 cbu4d94t\"\u003E\r\n                                \u003Cdiv class=\"rq0escxv l9j0dhe7 du4w35lb\"\u003E\r\n                                    \u003Cdiv class=\"j83agx80 l9j0dhe7 k4urcfbm\"\u003E\r\n                                        \u003Cdiv class=\"rq0escxv l9j0dhe7 du4w35lb hybvsw6c ue3kfks5 pw54ja7n uo3d90p7 l82x9zwi ni8dbmo4 stjgntxs k4urcfbm sbcfpzgs\" style=\"border-radius: max(0px, min(8px, ((100vw - 4px) - 100%) * 9999)) \u002F 8px;\"\u003E\r\n                                            \u003Cdiv\u003E\r\n                                                \u003Cdiv\u003E\u003C\u002Fdiv\u003E\r\n                                                \u003Cdiv\u003E\r\n                                                    \u003Cdiv\u003E\r\n                                                        \u003Cdiv\u003E\u003C\u002Fdiv\u003E\r\n                                                        \u003Cdiv\u003E\r\n                                                            \u003Cdiv class=\"pybr56ya dati1w0a hv4rvrfc n851cfcs btwxx1t3 j83agx80 ll8tlv6m\"\u003E\r\n                                                                \u003Cdiv class=\"oi9244e8 do00u71z j83agx80\"\u003E\r\n                                                                    \u003Cdiv class=\"nc684nl6\"\u003E\r\n                                                                        \u003Cdiv class=\"q676j6op qypqp5cg\"\u003E\u003Cobject type=\"nested\u002Fpressable\"\u003E\u003Ca class=\"oajrlxb2 gs1a9yip g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv nhd2j8a9 q9uorilb mg4g778l btwxx1t3 pfnyh3mw p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo hpfvmrgz jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso l9j0dhe7 i1ao9s8h esuyzwwr f1sip0of du4w35lb lzcic4wl abiwlrkh p8dawk7l oo9gr5id\" href=${\"https:\u002F\u002Fwww.facebook.com\u002F\"+parameters.FacebookURL} role=\"link\" tabindex=\"0\" target=\"_blank\" name=\"fbpage\"\u003E\r\n                                                                                    \u003Cdiv class=\"q9uorilb l9j0dhe7 pzggbiyp du4w35lb\"\u003E\u003Csvg class=\"pzggbiyp\" data-visualcompletion=\"ignore-dynamic\" role=\"none\" style=\"height: 40px; width: 40px;\"\u003E\r\n                                                                                            \u003Cmask id=\"jsc_c_t\"\u003E\r\n                                                                                                \u003Ccircle cx=\"20\" cy=\"20\" fill=\"white\" r=\"20\"\u003E\u003C\u002Fcircle\u003E\r\n                                                                                            \u003C\u002Fmask\u003E\r\n                                                                                            \u003Cg mask=\"url(#jsc_c_t)\"\u003E\r\n                                                                                                \u003Cimage x=\"0\" y=\"0\" height=\"100%\" preserveAspectRatio=\"xMidYMid slice\" width=\"100%\" xlink:href=${\"static\u002F\"+parameters.Logo} style=\"height: 40px; width: 40px;\"\u003E\u003C\u002Fimage\u003E\r\n                                                                                                \u003Ccircle class=\"mlqo0dh0 georvekb s6kb5r3f\" cx=\"20\" cy=\"20\" r=\"20\"\u003E\u003C\u002Fcircle\u003E\r\n                                                                                            \u003C\u002Fg\u003E\r\n                                                                                        \u003C\u002Fsvg\u003E\r\n                                                                                        \u003Cdiv class=\"s45kfl79 emlxlaya bkmhp75w spb7xbtv i09qtzwb n7fi1qx3 b5wmifdl hzruof5a pmk7jnqg j9ispegn kr520xx4 c5ndavph art1omkt ot9fgl3s\" data-visualcompletion=\"ignore\"\u003E\u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fa\u003E\u003C\u002Fobject\u003E\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E\r\n                                                                    \u003C\u002Fdiv\u003E\r\n                                                                \u003C\u002Fdiv\u003E\r\n                                                                \u003Cdiv class=\"buofh1pr\"\u003E\r\n                                                                    \u003Cdiv class=\"j83agx80 cbu4d94t ew0dbk1b irj2b8pg\"\u003E\r\n                                                                        \u003Cdiv class=\"qzhwtbm6 knvmm38d\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v knj5qynh m9osqain hzawbc8m\" dir=\"auto\"\u003E\r\n                                                                                \u003Ch2 id=\"jsc_c_n\" class=\"gmql0nx0 l94mrbxd p1ri9a11 lzcic4wl aahdfvyu hzawbc8m\" dir=\"auto\"\u003E\r\n                                                                                    \u003Cdiv class=\"nc684nl6\"\u003E\u003Ca class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl oo9gr5id gpro0wi8 lrazzd5p\" href=${\"https:\u002F\u002Fwww.facebook.com\u002F\"+parameters.FacebookURL} role=\"link\" tabindex=\"0\" target=\"_blank\" name=\"fbpage\"\u003E\u003Cstrong\u003E\u003Cspan\u003E${parameters.Facebook}\u003C\u002Fspan\u003E\u003C\u002Fstrong\u003E\u003C!--span\u003E \u003Ci class=\"img sp_YQP1I8yDIZ8 sx_84480b\"\u003E\u003C\u002Fi\u003E\u003C\u002Fspan--\u003E\u003C\u002Fa\u003E\u003C\u002Fdiv\u003E\u003Cspan class=\"l9j0dhe7 h3qc4492\"\u003E&nbsp;\u003Cdiv class=\"nc684nl6\"\u003E\r\n                                                                                            \u003Cdiv class=\"oajrlxb2 gs1a9yip g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv bnpdmtie pq6dq46d mg4g778l btwxx1t3 pfnyh3mw p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo hpfvmrgz jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso l9j0dhe7 i1ao9s8h esuyzwwr f1sip0of du4w35lb lzcic4wl abiwlrkh p8dawk7l\" role=\"button\" tabindex=\"0\"\u003E\u003Cspan class=\"hrs1iv20 pq6dq46d\"\u003E\u003Cspan class=\"hidden\"\u003E\u003Ci class=\"sp_IJ2krMpsgey sx_b7b87f\" role=\"img\"\u003E\u003C\u002Fi\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\r\n                                                                                                \u003Cdiv class=\"s45kfl79 emlxlaya bkmhp75w spb7xbtv i09qtzwb n7fi1qx3 b5wmifdl hzruof5a pmk7jnqg j9ispegn kr520xx4 c5ndavph art1omkt ot9fgl3s\" data-visualcompletion=\"ignore\"\u003E\u003C\u002Fdiv\u003E\r\n                                                                                            \u003C\u002Fdiv\u003E\r\n                                                                                        \u003C\u002Fdiv\u003E\u003C\u002Fspan\u003E\r\n                                                                                \u003C\u002Fh2\u003E\r\n                                                                            \u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                        \u003Cdiv class=\"qzhwtbm6 knvmm38d\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d9wwppkn fe6kdd0r mau55g9w c8b282yb mdeji52x e9vueds3 j5wam9gi knj5qynh m9osqain hzawbc8m\" dir=\"auto\"\u003E\u003Cspan id=\"jsc_c_o\"\u003E\u003Cspan class=\"jpp8pzdo\"\u003E\u003Cspan\u003E\u003Cspan class=\"rfua0xdk pmk7jnqg stjgntxs ni8dbmo4 ay7djpcl q45zohi1\"\u003E&nbsp;\u003C\u002Fspan\u003E\u003Cspan aria-hidden=\"true\"\u003E · \u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003Cspan\u003E\u003Cspan class=\"tojvnm2t a6sixzi8 abs2jz4q a8s20v7p t1p8iaqh k5wvi7nf q3lfd5jv pk4s997a bipmatt0 cebpdrjk qowsmv63 owwhemhu dp1hu0rb dhp61c6y iyyx5f41\"\u003E\u003Ca class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 gpro0wi8 b1v8xokw\"\u003E\u003Cspan class=\"blurry-text noselect\"\u003E${parameters.Date} at 00:00\u003C\u002Fspan\u003E\u003C\u002Fa\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003Cspan class=\"jpp8pzdo\"\u003E\u003Cspan\u003E\u003Cspan class=\"rfua0xdk pmk7jnqg stjgntxs ni8dbmo4 ay7djpcl q45zohi1\"\u003E&nbsp;\u003C\u002Fspan\u003E\u003Cspan aria-hidden=\"true\"\u003E · \u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003Cspan class=\"g0qnabr5\"\u003E\u003Cspan class=\"tojvnm2t a6sixzi8 abs2jz4q a8s20v7p t1p8iaqh k5wvi7nf q3lfd5jv pk4s997a bipmatt0 cebpdrjk qowsmv63 owwhemhu dp1hu0rb dhp61c6y iyyx5f41\"\u003E\u003Cspan class=\"ormqv51v l9j0dhe7\"\u003E\u003Ci class=\"hu5pjgll m6k467ps sp_IJ2krMpsgey sx_9381ee\" role=\"img\"\u003E\u003C\u002Fi\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                    \u003C\u002Fdiv\u003E\r\n                                                                \u003C\u002Fdiv\u003E\r\n                                                                \u003Cdiv class=\"nqmvxvec j83agx80 jnigpg78 cxgpxx05 dflh9lhu sj5x9vvc scb9dxdr odw8uiq3\"\u003E\r\n                                                                    \u003Cdiv aria-expanded=\"false\" aria-haspopup=\"menu\" class=\"oajrlxb2 gs1a9yip g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv nhd2j8a9 pq6dq46d mg4g778l btwxx1t3 pfnyh3mw p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo hpfvmrgz jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso l9j0dhe7 i1ao9s8h esuyzwwr f1sip0of du4w35lb lzcic4wl abiwlrkh p8dawk7l dwo3fsh8 pzggbiyp pkj7ub1o bqnlxs5p kkg9azqs c24pa1uk ln9iyx3p fe6kdd0r ar1oviwq l10q8mi9 sq40qgkc s8quxz6p pdjglbur\" role=\"button\" tabindex=\"0\"\u003E\u003Ci class=\"hu5pjgll m6k467ps sp_IJ2krMpsgey sx_d6f5ed\"\u003E\u003C\u002Fi\u003E\r\n                                                                        \u003Cdiv class=\"s45kfl79 emlxlaya bkmhp75w spb7xbtv i09qtzwb n7fi1qx3 b5wmifdl hzruof5a pmk7jnqg j9ispegn kr520xx4 c5ndavph art1omkt ot9fgl3s\" data-visualcompletion=\"ignore\" style=\"bottom: -8px; left: -8px; right: -8px; top: -8px;\"\u003E\u003C\u002Fdiv\u003E\r\n                                                                    \u003C\u002Fdiv\u003E\r\n                                                                \u003C\u002Fdiv\u003E\r\n                                                            \u003C\u002Fdiv\u003E\r\n                                                        \u003C\u002Fdiv\u003E\r\n                                                        \u003Cdiv\u003E\r\n                                                            \u003Cdiv class=\"\" dir=\"auto\"\u003E\r\n                                                                \u003Cdiv class=\"ecm0bbzt hv4rvrfc ihqw7lf3 dati1w0a\" data-ad-comet-preview=\"message\" data-ad-preview=\"message\" id=\"jsc_c_p\"\u003E\r\n                                                                    \u003Cdiv class=\"j83agx80 cbu4d94t ew0dbk1b irj2b8pg\"\u003E\r\n                                                                        \u003Cdiv class=\"qzhwtbm6 knvmm38d\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v knj5qynh oo9gr5id hzawbc8m\" dir=\"auto\"\u003E\r\n                                                                                \u003Cdiv class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\"\u003E\r\n                                                                                    \u003Cdiv dir=\"auto\" style=\"text-align: start;\"\u003E${parameters.Caption}\u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fdiv\u003E\r\n                                                                            \u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                    \u003C\u002Fdiv\u003E\r\n                                                                \u003C\u002Fdiv\u003E\r\n                                                            \u003C\u002Fdiv\u003E\r\n                                                            \u003Cdiv class=\"l9j0dhe7\" id=\"jsc_c_q\"\u003E\r\n                                                                \u003Cdiv class=\"l9j0dhe7\"\u003E\r\n                                                                    \u003Cdiv\u003E\r\n                                                                        \u003Cdiv class=\"b3i9ofy5 l9j0dhe7\"\u003E\r\n                                                                            \u003Cdiv class=\"j83agx80 soycq5t1 ni8dbmo4 stjgntxs l9j0dhe7\"\u003E\u003Ca class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 gpro0wi8 datstx6m k4urcfbm\" href=${parameters.ArticleURL} rel=\"nofollow noopener\" role=\"link\" tabindex=\"0\" target=\"_blank\" name=\"articlepage\"\u003E\r\n                                                                                    \u003Cdiv class=\"bp9cbjyn cwj9ozl2 j83agx80 cbu4d94t ni8dbmo4 stjgntxs l9j0dhe7 k4urcfbm\"\u003E\r\n                                                                                        \u003Cdiv style=\"max-width: 100%; min-width: 500px; width: calc((100vh + -325px) * 1.91571);\"\u003E\r\n                                                                                            \u003Cdiv class=\"do00u71z ni8dbmo4 stjgntxs l9j0dhe7\" style=\"padding-top: 52.2%;\"\u003E\r\n                                                                                                \u003Cdiv class=\"pmk7jnqg kr520xx4\" style=\"height: 100%; left: 0%; width: calc(100%);\"\u003E\u003Cimg height=\"261\" width=\"500\" class=\"i09qtzwb n7fi1qx3 datstx6m pmk7jnqg j9ispegn kr520xx4 k4urcfbm bixrwtb6\" src=${parameters.Image}\u003E\u003C\u002Fdiv\u003E\r\n                                                                                            \u003C\u002Fdiv\u003E\r\n                                                                                        \u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                    \u003Cdiv class=\"i09qtzwb rq0escxv n7fi1qx3 pmk7jnqg j9ispegn kr520xx4 linmgsc8 opwvks06 hzruof5a\"\u003E\u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fa\u003E\u003C\u002Fdiv\u003E\r\n                                                                        \u003C\u002Fdiv\u003E\r\n                                                                    \u003C\u002Fdiv\u003E\r\n                                                                    \u003Cdiv class=\"o0s42vec pmk7jnqg ehxjyohh b4oskaiq\" id=\"info_button\"\u003E\r\n                                                                        \u003Cdiv role=\"tooltip\" id=\"info_aria\" class=\"j34wkznp qp9yad78 pmk7jnqg kr520xx4 hzruof5a\" style=\"transform: translate(0px, 0px) translate(-95%, -110%); display: none;\"\u003E\r\n                                                                            \u003Cspan class=\"__fb-light-mode  ms7hmo2b ue3kfks5 pw54ja7n uo3d90p7 l82x9zwi r92hip7p a8c37x1j dicw6rsg rs0gx3tq bkm5gps7 pedkr2u6 pybr56ya d1544ag0 f10w8fjw tw6a2znq l9j0dhe7 ijkhr0an art1omkt s13u9afw\"\u003E\r\n                                                                                \u003Cdiv class=\"j83agx80 cbu4d94t ew0dbk1b irj2b8pg\"\u003E\r\n                                                                                    \u003Cdiv class=\"qzhwtbm6 knvmm38d\"\u003E\r\n                                                                                        \u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d9wwppkn fe6kdd0r mau55g9w c8b282yb mdeji52x e9vueds3 j5wam9gi knj5qynh oo9gr5id hzawbc8m\" dir=\"auto\"\u003EShow more information about this link\r\n                                                                                        \u003C\u002Fspan\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fdiv\u003E\r\n                                                                            \u003C\u002Fspan\u003E\r\n                                                                        \u003C\u002Fdiv\u003E\r\n                                                                        \u003Cdiv class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 gpro0wi8\" id=\"contextbutton\" role=\"button\" tabindex=\"0\"\u003E\r\n                                                                            \u003Cdiv class=\"bp9cbjyn cwj9ozl2 opwvks06 hop1g133 linmgsc8 t63ysoy8 cmek9o9a p7f4f6cj c8oo3d72 r15kkdkt oo9gr5id j83agx80 mudddibn taijpn5t ciadx1gn\"\u003E\u003Cspan class=\"tojvnm2t a6sixzi8 abs2jz4q a8s20v7p t1p8iaqh k5wvi7nf q3lfd5jv pk4s997a bipmatt0 cebpdrjk qowsmv63 owwhemhu dp1hu0rb dhp61c6y iyyx5f41\" aria-describedby=\"info_aria\"\u003E\u003Ci class=\"hu5pjgll lzf7d6o1 sp_CaOautnkgPg sx_60cd14\"\u003E\u003C\u002Fi\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                        \u003C\u002Fdiv\u003E\r\n                                                                    \u003C\u002Fdiv\u003E\r\n                                                                \u003C\u002Fdiv\u003E\r\n                                                                \u003Cdiv class=\"stjgntxs ni8dbmo4\"\u003E\r\n                                                                    \u003Cdiv class=\"l9j0dhe7\"\u003E\u003Ca class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 a8c37x1j p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 p8dawk7l\" href=${parameters.ArticleURL} rel=\"nofollow noopener\" role=\"link\" tabindex=\"0\" target=\"_blank\" name=\"articlepage\"\u003E\r\n                                                                            \u003Cdiv class=\"b3i9ofy5 s1tcr66n l9j0dhe7 p8dawk7l\"\u003E\r\n                                                                                \u003Cdiv class=\"rq0escxv l9j0dhe7 du4w35lb j83agx80 pfnyh3mw i1fnvgqd bp9cbjyn owycx6da btwxx1t3 b5q2rw42 lq239pai f10w8fjw hv4rvrfc dati1w0a pybr56ya\"\u003E\r\n                                                                                    \u003Cdiv class=\"rq0escxv l9j0dhe7 du4w35lb d2edcug0 hpfvmrgz rj1gh0hx buofh1pr g5gj957u p8fzw8mz pcp91wgn\"\u003E\r\n                                                                                        \u003Cdiv class=\"bi6gxh9e sqxagodl\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d9wwppkn fe6kdd0r mau55g9w c8b282yb iv3no6db e9vueds3 j5wam9gi knj5qynh m9osqain\" dir=\"auto\"\u003E\u003Cspan class=\"a8c37x1j ni8dbmo4 stjgntxs l9j0dhe7 ltmttdrg g0qnabr5 ojkyduve\"\u003E${parameters.WebsiteURL}\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                        \u003Cdiv class=\"enqfppq2 muag1w35 ni8dbmo4 stjgntxs e5nlhep0 ecm0bbzt rq0escxv a5q79mjw r9c01rrb\"\u003E\r\n                                                                                            \u003Cdiv class=\"j83agx80 cbu4d94t ew0dbk1b irj2b8pg\"\u003E\r\n                                                                                                \u003Cdiv class=\"qzhwtbm6 knvmm38d\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db a5q79mjw g1cxx5fr lrazzd5p oo9gr5id hzawbc8m\" dir=\"auto\"\u003E\u003Cspan class=\"a8c37x1j ni8dbmo4 stjgntxs l9j0dhe7 ojkyduve\" style=\"-webkit-box-orient: vertical; -webkit-line-clamp: 2; display: -webkit-box;\"\u003E\u003Cspan dir=\"auto\"\u003E${parameters.Title}\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                                \u003Cdiv class=\"qzhwtbm6 knvmm38d\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v knj5qynh m9osqain hzawbc8m\" dir=\"auto\"\u003E\u003Cspan class=\"a8c37x1j ni8dbmo4 stjgntxs l9j0dhe7 ltmttdrg g0qnabr5\"\u003E\u003Cspan\u003E${parameters.Subtitle}\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                            \u003C\u002Fdiv\u003E\r\n                                                                                        \u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                    \u003Cdiv class=\"rq0escxv l9j0dhe7 du4w35lb j83agx80 cbu4d94t pfnyh3mw d2edcug0 hpfvmrgz p8fzw8mz pcp91wgn\"\u003E\u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fdiv\u003E\r\n                                                                            \u003C\u002Fdiv\u003E\r\n                                                                        \u003C\u002Fa\u003E\u003C\u002Fdiv\u003E\r\n                                                                \u003C\u002Fdiv\u003E\r\n                                                            \u003C\u002Fdiv\u003E\r\n                                                        \u003C\u002Fdiv\u003E\r\n                                                        \u003Cdiv\u003E\r\n                                                            \u003Cdiv class=\"stjgntxs ni8dbmo4 l82x9zwi uo3d90p7 h905i5nu monazrh9\" data-visualcompletion=\"ignore-dynamic\"\u003E\r\n                                                                \u003Cdiv\u003E\r\n                                                                    \u003Cdiv\u003E\r\n                                                                        \u003Cdiv\u003E\r\n                                                                            \u003Cdiv class=\"l9j0dhe7\"\u003E\r\n                                                                                \u003Cdiv class=\"bp9cbjyn m9osqain j83agx80 jq4qci2q bkfpd7mw a3bd9o3v kvgmc6g5 wkznzc2l oygrvhab dhix69tm jktsbyx5 rz4wbd8a osnr6wyh a8nywdso s1tcr66n\"\u003E\r\n                                                                                    \u003Cdiv class=\"bp9cbjyn j83agx80 buofh1pr ni8dbmo4 stjgntxs\"\u003E\u003Cspan role=\"toolbar\"\u003E\u003Cspan id=\"jsc_c_s\" class=\"bp9cbjyn j83agx80 b3onmgus\"\u003E\u003Cspan class=\"np69z8it et4y5ytx j7g94pet b74d5cxt qw6c0r16 kb8x4rkr ed597pkb omcyoz59 goun2846 ccm00jje s44p3ltw mk2mc5f4 qxh1up0x qtyiw8t4 tpcyxxvw k0bpgpbk hm271qws rl04r1d5 l9j0dhe7 ov9facns kavbgo14\"\u003E\u003Cspan class=\"t0qjyqq4 jos75b7i j6sty90h kv0toi1t q9uorilb hm271qws ov9facns\"\u003E\u003Cspan class=\"tojvnm2t a6sixzi8 abs2jz4q a8s20v7p t1p8iaqh k5wvi7nf q3lfd5jv pk4s997a bipmatt0 cebpdrjk qowsmv63 owwhemhu dp1hu0rb dhp61c6y iyyx5f41\"\u003E\r\n                                                                                                            \u003Cdiv class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l\" role=\"button\" tabindex=\"0\"\u003E\u003Cimg class=\"j1lvzwm4\" height=\"18\" src=\"data:image\u002Fsvg+xml,%3csvg xmlns='http:\u002F\u002Fwww.w3.org\u002F2000\u002Fsvg' xmlns:xlink='http:\u002F\u002Fwww.w3.org\u002F1999\u002Fxlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'\u002F%3e%3cstop offset='100%25' stop-color='%230062DF'\u002F%3e%3c\u002FlinearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'\u002F%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'\u002F%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'\u002F%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'\u002F%3e%3c\u002Ffilter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'\u002F%3e%3c\u002Fdefs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'\u002F%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'\u002F%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'\u002F%3e%3c\u002Fg%3e%3c\u002Fsvg%3e\" width=\"18\"\u003E\u003C\u002Fdiv\u003E\r\n                                                                                                        \u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003Cspan class=\"np69z8it et4y5ytx j7g94pet b74d5cxt qw6c0r16 kb8x4rkr ed597pkb omcyoz59 goun2846 ccm00jje s44p3ltw mk2mc5f4 qxh1up0x qtyiw8t4 tpcyxxvw k0bpgpbk hm271qws rl04r1d5 l9j0dhe7 ov9facns tkr6xdv7\"\u003E\u003Cspan class=\"t0qjyqq4 jos75b7i j6sty90h kv0toi1t q9uorilb hm271qws ov9facns\"\u003E\u003Cspan class=\"tojvnm2t a6sixzi8 abs2jz4q a8s20v7p t1p8iaqh k5wvi7nf q3lfd5jv pk4s997a bipmatt0 cebpdrjk qowsmv63 owwhemhu dp1hu0rb dhp61c6y iyyx5f41\"\u003E\r\n                                                                                                            \u003Cdiv class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l\" role=\"button\" tabindex=\"0\"\u003E\u003Cimg class=\"j1lvzwm4\" height=\"18\" src=\"data:image\u002Fsvg+xml,%3csvg xmlns='http:\u002F\u002Fwww.w3.org\u002F2000\u002Fsvg' xmlns:xlink='http:\u002F\u002Fwww.w3.org\u002F1999\u002Fxlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='10.25%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23FEEA70'\u002F%3e%3cstop offset='100%25' stop-color='%23F69B30'\u002F%3e%3c\u002FlinearGradient%3e%3clinearGradient id='d' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23472315'\u002F%3e%3cstop offset='100%25' stop-color='%238B3A0E'\u002F%3e%3c\u002FlinearGradient%3e%3clinearGradient id='e' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23191A33'\u002F%3e%3cstop offset='87.162%25' stop-color='%233B426A'\u002F%3e%3c\u002FlinearGradient%3e%3clinearGradient id='j' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23E78E0D'\u002F%3e%3cstop offset='100%25' stop-color='%23CB6000'\u002F%3e%3c\u002FlinearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'\u002F%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'\u002F%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'\u002F%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0.921365489 0 0 0 0 0.460682745 0 0 0 0 0 0 0 0 0.35 0'\u002F%3e%3c\u002Ffilter%3e%3cfilter id='g' width='111.1%25' height='133.3%25' x='-5.6%25' y='-16.7%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='.5'\u002F%3e%3cfeOffset in='shadowBlurInner1' result='shadowOffsetInner1'\u002F%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'\u002F%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0.0980392157 0 0 0 0 0.101960784 0 0 0 0 0.2 0 0 0 0.819684222 0'\u002F%3e%3c\u002Ffilter%3e%3cfilter id='h' width='204%25' height='927.2%25' x='-52.1%25' y='-333.3%25' filterUnits='objectBoundingBox'%3e%3cfeOffset dy='1' in='SourceAlpha' result='shadowOffsetOuter1'\u002F%3e%3cfeGaussianBlur in='shadowOffsetOuter1' result='shadowBlurOuter1' stdDeviation='1.5'\u002F%3e%3cfeColorMatrix in='shadowBlurOuter1' values='0 0 0 0 0.803921569 0 0 0 0 0.388235294 0 0 0 0 0.00392156863 0 0 0 0.14567854 0'\u002F%3e%3c\u002Ffilter%3e%3cpath id='b' d='M16 8A8 8 0 110 8a8 8 0 0116 0'\u002F%3e%3cpath id='f' d='M3.5 5.5c0-.828.559-1.5 1.25-1.5S6 4.672 6 5.5C6 6.329 5.441 7 4.75 7S3.5 6.329 3.5 5.5zm6.5 0c0-.828.56-1.5 1.25-1.5.691 0 1.25.672 1.25 1.5 0 .829-.559 1.5-1.25 1.5C10.56 7 10 6.329 10 5.5z'\u002F%3e%3cpath id='i' d='M11.068 1.696c.052-.005.104-.007.157-.007.487 0 .99.204 1.372.562a.368.368 0 01.022.51.344.344 0 01-.496.024c-.275-.259-.656-.4-.992-.369a.8.8 0 00-.59.331.346.346 0 01-.491.068.368.368 0 01-.067-.507 1.49 1.49 0 011.085-.612zm-7.665.555a2.042 2.042 0 011.372-.562 1.491 1.491 0 011.242.619.369.369 0 01-.066.507.347.347 0 01-.492-.068.801.801 0 00-.59-.331c-.335-.031-.717.11-.992.369a.344.344 0 01-.496-.024.368.368 0 01.022-.51z'\u002F%3e%3c\u002Fdefs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'\u002F%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'\u002F%3e%3cpath fill='url(%23d)' d='M5.643 10.888C5.485 12.733 6.369 14 8 14c1.63 0 2.515-1.267 2.357-3.112C10.2 9.042 9.242 8 8 8c-1.242 0-2.2 1.042-2.357 2.888'\u002F%3e%3cuse fill='url(%23e)' xlink:href='%23f'\u002F%3e%3cuse fill='black' filter='url(%23g)' xlink:href='%23f'\u002F%3e%3cpath fill='%234E506A' d='M4.481 4.567c.186.042.29.252.232.469-.057.218-.254.36-.44.318-.186-.042-.29-.252-.232-.47.057-.216.254-.36.44-.317zm6.658.063c.206.047.322.28.258.52-.064.243-.282.4-.489.354-.206-.046-.322-.28-.258-.521.063-.242.282-.4.49-.353z'\u002F%3e%3cuse fill='black' filter='url(%23h)' xlink:href='%23i'\u002F%3e%3cuse fill='url(%23j)' xlink:href='%23i'\u002F%3e%3c\u002Fg%3e%3c\u002Fsvg%3e\" width=\"18\"\u003E\u003C\u002Fdiv\u003E\r\n                                                                                                        \u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C!--span class=\"np69z8it et4y5ytx j7g94pet b74d5cxt qw6c0r16 kb8x4rkr ed597pkb omcyoz59 goun2846 ccm00jje s44p3ltw mk2mc5f4 qxh1up0x qtyiw8t4 tpcyxxvw k0bpgpbk hm271qws rl04r1d5 l9j0dhe7 ov9facns du4w35lb\"\u003E\u003Cspan class=\"t0qjyqq4 jos75b7i j6sty90h kv0toi1t q9uorilb hm271qws ov9facns\"\u003E\u003Cspan class=\"tojvnm2t a6sixzi8 abs2jz4q a8s20v7p t1p8iaqh k5wvi7nf q3lfd5jv pk4s997a bipmatt0 cebpdrjk qowsmv63 owwhemhu dp1hu0rb dhp61c6y iyyx5f41\"\u003E\r\n                                                                                                            \u003Cdiv class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l\" role=\"button\" tabindex=\"0\"\u003E\u003Cimg class=\"j1lvzwm4\" height=\"18\" src=\"data:image\u002Fsvg+xml,%3csvg xmlns='http:\u002F\u002Fwww.w3.org\u002F2000\u002Fsvg' xmlns:xlink='http:\u002F\u002Fwww.w3.org\u002F1999\u002Fxlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='10.25%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23FEEA70'\u002F%3e%3cstop offset='100%25' stop-color='%23F69B30'\u002F%3e%3c\u002FlinearGradient%3e%3clinearGradient id='d' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23472315'\u002F%3e%3cstop offset='100%25' stop-color='%238B3A0E'\u002F%3e%3c\u002FlinearGradient%3e%3clinearGradient id='e' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23191A33'\u002F%3e%3cstop offset='87.162%25' stop-color='%233B426A'\u002F%3e%3c\u002FlinearGradient%3e%3clinearGradient id='h' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23E78E0D'\u002F%3e%3cstop offset='100%25' stop-color='%23CB6000'\u002F%3e%3c\u002FlinearGradient%3e%3clinearGradient id='i' x1='50%25' x2='50%25' y1='81.899%25' y2='17.94%25'%3e%3cstop offset='0%25' stop-color='%2335CAFC'\u002F%3e%3cstop offset='100%25' stop-color='%23007EDB'\u002F%3e%3c\u002FlinearGradient%3e%3clinearGradient id='j' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%236AE1FF' stop-opacity='.287'\u002F%3e%3cstop offset='100%25' stop-color='%23A8E3FF' stop-opacity='.799'\u002F%3e%3c\u002FlinearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'\u002F%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'\u002F%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'\u002F%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0.921365489 0 0 0 0 0.460682745 0 0 0 0 0 0 0 0 0.35 0'\u002F%3e%3c\u002Ffilter%3e%3cfilter id='g' width='111.4%25' height='137.5%25' x='-5.7%25' y='-18.8%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='.5'\u002F%3e%3cfeOffset in='shadowBlurInner1' result='shadowOffsetInner1'\u002F%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'\u002F%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0.0411226772 0 0 0 0 0.0430885485 0 0 0 0 0.0922353316 0 0 0 0.819684222 0'\u002F%3e%3c\u002Ffilter%3e%3cpath id='b' d='M16 8A8 8 0 110 8a8 8 0 0116 0'\u002F%3e%3cpath id='f' d='M3.599 8.8c0-.81.509-1.466 1.134-1.466.627 0 1.134.656 1.134 1.466 0 .338-.089.65-.238.898a.492.492 0 01-.301.225c-.14.037-.353.077-.595.077-.243 0-.453-.04-.595-.077a.49.49 0 01-.3-.225 1.741 1.741 0 01-.239-.898zm6.534 0c0-.81.508-1.466 1.133-1.466.627 0 1.134.656 1.134 1.466 0 .338-.089.65-.238.898a.49.49 0 01-.301.225 2.371 2.371 0 01-1.189 0 .49.49 0 01-.301-.225 1.74 1.74 0 01-.238-.898z'\u002F%3e%3c\u002Fdefs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'\u002F%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'\u002F%3e%3cpath fill='url(%23d)' d='M5.333 12.765c0 .137.094.235.25.235.351 0 .836-.625 2.417-.625s2.067.625 2.417.625c.156 0 .25-.098.25-.235C10.667 12.368 9.828 11 8 11c-1.828 0-2.667 1.368-2.667 1.765'\u002F%3e%3cuse fill='url(%23e)' xlink:href='%23f'\u002F%3e%3cuse fill='black' filter='url(%23g)' xlink:href='%23f'\u002F%3e%3cpath fill='%234E506A' d='M4.616 7.986c.128.125.136.372.017.551-.12.178-.32.222-.448.096-.128-.125-.135-.372-.017-.55.12-.179.32-.222.448-.097zm6.489 0c.128.125.136.372.018.551-.12.178-.32.222-.45.096-.127-.125-.134-.372-.015-.55.119-.179.319-.222.447-.097z'\u002F%3e%3cpath fill='url(%23h)' d='M4.157 5.153c.332-.153.596-.219.801-.219.277 0 .451.119.55.306.175.329.096.401-.198.459-1.106.224-2.217.942-2.699 1.39-.301.28-.589-.03-.436-.274.154-.244.774-1.105 1.982-1.662zm6.335.087c.099-.187.273-.306.55-.306.206 0 .469.066.801.219 1.208.557 1.828 1.418 1.981 1.662.153.244-.134.554-.435.274-.483-.448-1.593-1.166-2.7-1.39-.294-.058-.371-.13-.197-.459z'\u002F%3e%3cpath fill='url(%23i)' d='M13.5 16c-.828 0-1.5-.748-1.5-1.671 0-.922.356-1.545.643-2.147.598-1.258.716-1.432.857-1.432.141 0 .259.174.857 1.432.287.602.643 1.225.643 2.147 0 .923-.672 1.671-1.5 1.671'\u002F%3e%3cpath fill='url(%23j)' d='M13.5 13.606c-.328 0-.594-.296-.594-.66 0-.366.141-.613.255-.852.236-.498.283-.566.339-.566.056 0 .103.068.339.566.114.24.255.486.255.851s-.266.661-.594.661'\u002F%3e%3c\u002Fg%3e%3c\u002Fsvg%3e\" width=\"18\"\u003E\u003C\u002Fdiv\u003E\r\n                                                                                                        \u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan--\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\r\n                                                                                        \u003Cdiv class=\"\"\u003E\u003Cspan class=\"tojvnm2t a6sixzi8 abs2jz4q a8s20v7p t1p8iaqh k5wvi7nf q3lfd5jv pk4s997a bipmatt0 cebpdrjk qowsmv63 owwhemhu dp1hu0rb dhp61c6y iyyx5f41\"\u003E\r\n                                                                                                \u003Cdiv class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 a8c37x1j p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l gmql0nx0 ce9h75a5 ni8dbmo4 stjgntxs\" role=\"button\" tabindex=\"0\"\u003E\u003Cspan aria-hidden=\"true\" class=\"bzsjyuwj ni8dbmo4 stjgntxs ltmttdrg gjzvkazv\"\u003E\u003Cspan\u003E\u003Cspan class=\"gpro0wi8 pcp91wgn blurry-text noselect\"\u003E00\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E \u003Cspan class=\"gpro0wi8 cwj9ozl2 bzsjyuwj ja2t1vim\"\u003E\u003Cspan\u003E\u003Cspan class=\"pcp91wgn blurry-text noselect\"\u003E00\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                            \u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                    \u003Cdiv class=\"bp9cbjyn j83agx80 pfnyh3mw p1ueia1e\"\u003E\r\n                                                                                        \u003Cdiv class=\"gtad4xkn\"\u003E\r\n                                                                                            \u003Cdiv class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh gpro0wi8 dwo3fsh8 ow4ym5g4 auili1gw du4w35lb gmql0nx0\" id=\"jsc_c_r\" role=\"button\" tabindex=\"0\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v knj5qynh m9osqain\" dir=\"auto\"\u003E\u003Cspan class=\"blurry-text noselect\"\u003E00\u003C\u002Fspan\u003E Comments\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                        \u003C\u002Fdiv\u003E\r\n                                                                                        \u003Cdiv class=\"gtad4xkn\"\u003E\u003Cspan class=\"tojvnm2t a6sixzi8 abs2jz4q a8s20v7p t1p8iaqh k5wvi7nf q3lfd5jv pk4s997a bipmatt0 cebpdrjk qowsmv63 owwhemhu dp1hu0rb dhp61c6y iyyx5f41\"\u003E\r\n                                                                                                \u003Cdiv class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh gpro0wi8 dwo3fsh8 ow4ym5g4 auili1gw du4w35lb gmql0nx0\" role=\"button\" tabindex=\"0\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v knj5qynh m9osqain\" dir=\"auto\"\u003E\u003Cspan class=\"blurry-text noselect\"\u003E00\u003C\u002Fspan\u003E Shares\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                            \u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fdiv\u003E\r\n                                                                            \u003C\u002Fdiv\u003E\r\n                                                                            \u003Cdiv class=\"a8nywdso e5nlhep0 rz4wbd8a ecm0bbzt dhix69tm oygrvhab wkznzc2l kvgmc6g5 k7cz35w2 jq4qci2q j83agx80\"\u003E\u003Cspan class=\" _18vi\"\u003E\r\n                                                                                    \u003Cdiv data-visualcompletion=\"ignore-dynamic\" class=\"l9j0dhe7 k4urcfbm\"\u003E\r\n                                                                                        \u003Cdiv class=\"l9j0dhe7\"\u003E\u003Cspan\u003E\r\n                                                                                                \u003Cdiv class=\"oajrlxb2 bp9cbjyn g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv nhd2j8a9 j83agx80 mg4g778l btwxx1t3 pfnyh3mw p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo hpfvmrgz jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso l9j0dhe7 i1ao9s8h esuyzwwr gokke00a du4w35lb lzcic4wl abiwlrkh p8dawk7l buofh1pr taijpn5t\" role=\"button\" tabindex=\"0\"\u003E\r\n                                                                                                    \u003Cdiv class=\"e71nayrh  _18vj\"\u003E\u003Cspan class=\"q9uorilb sf5mxxl7 bdca9zbp\"\u003E\u003Cspan class=\" pq6dq46d\"\u003E\u003Cspan class=\"pq6dq46d fv0vnmcu\"\u003E\u003Ci class=\"sp_KED-tFDJUQ8 sx_229595\" role=\"img\"\u003E\u003C\u002Fi\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fspan\u003ELike\u003C\u002Fdiv\u003E\r\n                                                                                                \u003C\u002Fdiv\u003E\r\n                                                                                                \u003Cdiv class=\"oajrlxb2 gs1a9yip g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv nhd2j8a9 pq6dq46d mg4g778l btwxx1t3 pfnyh3mw p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo hpfvmrgz b4ylihy8 rz4wbd8a b40mr0ww a8nywdso pmk7jnqg i1ao9s8h esuyzwwr f1sip0of du4w35lb lzcic4wl abiwlrkh p8dawk7l n7fi1qx3 maw0qe7g q45zohi1 g0aa4cga\" role=\"button\" tabindex=\"0\"\u003E\u003Ci class=\"hu5pjgll m6k467ps sp_IJ2krMpsgey sx_72ce01\"\u003E\u003C\u002Fi\u003E\r\n                                                                                                    \u003Cdiv class=\"n00je7tq arfg74bv qs9ysxi8 k77z8yql i09qtzwb n7fi1qx3 b5wmifdl hzruof5a pmk7jnqg j9ispegn kr520xx4 c5ndavph art1omkt ot9fgl3s\" data-visualcompletion=\"ignore\"\u003E\u003C\u002Fdiv\u003E\r\n                                                                                                \u003C\u002Fdiv\u003E\r\n                                                                                            \u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fspan\u003E\u003Cspan class=\" _18vi\"\u003E\r\n                                                                                    \u003Cdiv class=\"oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv n05y2jgg hbms080z p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h vul9dhcy f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l f49446kz  _666h  _18vj _18vk\" role=\"button\" tabindex=\"0\"\u003EComment\u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fspan\u003E\r\n                                                                                \u003Cdiv class=\"oajrlxb2 bp9cbjyn g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv nhd2j8a9 j83agx80 rj1gh0hx btwxx1t3 pfnyh3mw p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo hpfvmrgz jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso l9j0dhe7 i1ao9s8h esuyzwwr f1sip0of du4w35lb lzcic4wl abiwlrkh p8dawk7l buofh1pr k7cz35w2 taijpn5t ms05siws flx89l3n ogy3fsii\" role=\"button\" tabindex=\"0\"\u003E\r\n                                                                                    \u003Cdiv class=\"rq0escxv l9j0dhe7 du4w35lb j83agx80 pfnyh3mw taijpn5t bp9cbjyn owycx6da btwxx1t3\"\u003E\r\n                                                                                        \u003Cdiv class=\"rq0escxv l9j0dhe7 du4w35lb j83agx80 cbu4d94t pfnyh3mw d2edcug0 hpfvmrgz p8fzw8mz\"\u003E\u003Ci class=\"hu5pjgll m6k467ps sp_CaOautnkgPg sx_d053b6\"\u003E\u003C\u002Fi\u003E\u003C\u002Fdiv\u003E\r\n                                                                                        \u003Cdiv class=\"rq0escxv l9j0dhe7 du4w35lb j83agx80 cbu4d94t pfnyh3mw d2edcug0 hpfvmrgz\"\u003E\u003Cspan class=\"d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v lrazzd5p m9osqain\" dir=\"auto\"\u003E\u003Cspan class=\"a8c37x1j ni8dbmo4 stjgntxs l9j0dhe7 ltmttdrg g0qnabr5\"\u003EShare\u003C\u002Fspan\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                    \u003Cdiv class=\"n00je7tq arfg74bv qs9ysxi8 k77z8yql i09qtzwb n7fi1qx3 b5wmifdl hzruof5a pmk7jnqg j9ispegn kr520xx4 c5ndavph art1omkt ot9fgl3s\" data-visualcompletion=\"ignore\" style=\"border-radius: 4px;\"\u003E\r\n                                                                                    \u003C\u002Fdiv\u003E\r\n                                                                                \u003C\u002Fdiv\u003E\r\n                                                                            \u003C\u002Fdiv\u003E\r\n                                                                        \u003C\u002Fdiv\u003E\r\n                                                                    \u003C\u002Fdiv\u003E\r\n                                                                \u003C\u002Fdiv\u003E\r\n                                                            \u003C\u002Fdiv\u003E\r\n                                                        \u003C\u002Fdiv\u003E\r\n                                                    \u003C\u002Fdiv\u003E\r\n                                                \u003C\u002Fdiv\u003E\r\n                                            \u003C\u002Fdiv\u003E\r\n                                        \u003C\u002Fdiv\u003E\r\n                                    \u003C\u002Fdiv\u003E\r\n                                \u003C\u002Fdiv\u003E\r\n                            \u003C\u002Fdiv\u003E\r\n                        \u003C\u002Fdiv\u003E\r\n                    \u003C\u002Fdiv\u003E\r\n                \u003C\u002Fdiv\u003E\r\n            \u003C\u002Fdiv\u003E\r\n        \u003C\u002Fdiv\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\u003C\u002Fdiv\u003E",
        "correctResponse": "proceed"
      }
    }
  ]
})

// Let's go!
study.run()